﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorWeightedDefault.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    ///     Der Standart Gewichtungs Validator wiederhollt von Rechts nach links, mit der annahme die nummer ganz Rechts ist
    ///     die Prüfziffer.
    ///     Jede Nummer wird mit der Gewichtung multipliziert und zu m_aux hinzu addiert.
    ///     Am ende _modulo minus der letzten nummer von m_aux wird verglichen mit der prüffziffer modulo 10
    /// </summary>
    internal class ValidatorWeightedDefault : ValidatorWeighted
    {
        public ValidatorWeightedDefault(int modulo, params byte[] weight)
            : this(modulo, (int)-2, weight)
        {
        }

        public ValidatorWeightedDefault(int modulo, int start, params byte[] weight)
            : this(modulo, start, (int)0, weight)
        {
        }

        public ValidatorWeightedDefault(int modulo, int start, int end, params byte[] weight)
            : this(modulo, start, end, -1, weight)
        {
        }

        public ValidatorWeightedDefault(int modulo, int start, int end, int checkNumberPos, params byte[] weight)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (Modulo - Int32.Parse(m_aux.ToString().Substring(m_aux.ToString().Length - 1))) % 10;
            return check == getCheckNumber(accountId);
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += number * weight;
        }
    }
}