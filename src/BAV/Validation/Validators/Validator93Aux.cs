﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator93Aux : Validator
    {
        Validator06 m_validator_06a;
        Validator06 m_validator_06b;
        public Validator93Aux()
            : this(11)
        {
        }
        public Validator93Aux(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 }))
        {
        }
        public Validator93Aux(Int32 modulo, System.Collections.ArrayList weight)
        {
            m_validator_06a = new Validator06(modulo, weight, 4, 0, 5);
            m_validator_06b = new Validator06(modulo, weight, -2, 4);
        }
        public override bool IsValid(Account account)
        {

            string accountID = this.PreNulls(account);
            if (accountID == "")
                return false;


            if (accountID.Substring(0, 4) == "0000")
            {
                return this.m_validator_06b.IsValid(account);
            }
            else
            {
                return this.m_validator_06a.IsValid(account);
            }

        }
    }
}
