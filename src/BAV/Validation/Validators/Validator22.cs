﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator22.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator22 : ValidatorWeightedDefault
    {
        public Validator22()
            : this(10)
        {
        }

        public Validator22(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(3);
            _weight.Add(1);
        }

        public Validator22(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator22(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator22(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator22(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = 10 - Int32.Parse(m_aux.ToString().Substring(m_aux.ToString().Length - 1));
            return check == getCheckNumber(accountId);
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux = (number*weight)%Modulo;
        }
    }
}