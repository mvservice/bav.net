﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator90 : ValidatorMulti
    {
        private Validator06 m_sachValidator;

        public Validator90()
        {
            this._validators.Add(new Validator32(11, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6, 7 })));
            this._validators.Add(new Validator33(11, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 })));
            this._validators.Add(new Validator33(7, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 })));
            this._validators.Add(new Validator33(9, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 })));
            this._validators.Add(new Validator33(10, new System.Collections.ArrayList(new Object[] { 2, 1 })));


            this.m_sachValidator = new Validator06(11, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6, 7, 8 }), -2, 2);
        }

        protected override bool ContinueCondition(Account account)
        {
            switch (_current)
            {
                case 1:
                    string check = account.Number.Substring(account.Number.Length - 1);
                    if (Int32.Parse(check) >= 7)
                    {
                        _current++;
                        return true;
                    }
                    break;
            }
            return true;
        }
        public override bool IsValid(Account account)
        {
            string accountID = this.PreNulls(account);
            if (accountID == "")
                return false;
            if (accountID[2] == '9')
                return this.m_sachValidator.IsValid(account);
            else
                return base.IsValid(account);
        }


    }
}
