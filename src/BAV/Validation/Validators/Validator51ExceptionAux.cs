﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator51ExceptionAux : ValidatorWeightedDefault
    {
        public Validator51ExceptionAux()
            : this(11)
        {
        }
        public Validator51ExceptionAux(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(2);
            this._weight.Add(3);
            this._weight.Add(4);
            this._weight.Add(5);
            this._weight.Add(6);
            this._weight.Add(7);
        }
        public Validator51ExceptionAux(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }
        public Validator51ExceptionAux(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }
        public Validator51ExceptionAux(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator51ExceptionAux(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }
        protected override bool IsValid(string accountId)
        {
            Int32 check = this.m_aux % this.Modulo;
            switch (check)
            {
                case 1:
                case 0:
                    check = 0;
                    break;
                default:
                    check = this.Modulo - check;
                    break;
            }
            return check == this.getCheckNumber(accountId);
        }
    }
}
