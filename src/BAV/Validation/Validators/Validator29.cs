﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator29.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator29 : ValidatorTransformation
    {
        private readonly Int32 m_modulo;

        public Validator29()
            : this(10)
        {
        }

        public Validator29(
            Int32 modulo
            )
        {
            m_modulo = modulo;
            _matrix = new System.Collections.Hashtable();
            System.Collections.Hashtable row = new System.Collections.Hashtable();
            System.Collections.Hashtable row2 = new System.Collections.Hashtable();
            System.Collections.Hashtable row3 = new System.Collections.Hashtable();
            System.Collections.Hashtable row4 = new System.Collections.Hashtable();
            row.Add(0, 0);
            row.Add(1, 1);
            row.Add(2, 5);
            row.Add(3, 9);
            row.Add(4, 3);
            row.Add(5, 7);
            row.Add(6, 4);
            row.Add(7, 8);
            row.Add(8, 2);
            row.Add(9, 6);
            _matrix.Add(0, row);
            row2.Add(0, 0);
            row2.Add(1, 1);
            row2.Add(2, 7);
            row2.Add(3, 6);
            row2.Add(4, 9);
            row2.Add(5, 8);
            row2.Add(6, 3);
            row2.Add(7, 2);
            row2.Add(8, 5);
            row2.Add(9, 4);
            _matrix.Add(1, row2);
            row3.Add(0, 0);
            row3.Add(1, 1);
            row3.Add(2, 8);
            row3.Add(3, 4);
            row3.Add(4, 6);
            row3.Add(5, 2);
            row3.Add(6, 9);
            row3.Add(7, 5);
            row3.Add(8, 7);
            row3.Add(9, 3);
            _matrix.Add(2, row3);
            row4.Add(0, 0);
            row4.Add(1, 1);
            row4.Add(2, 2);
            row4.Add(3, 3);
            row4.Add(4, 4);
            row4.Add(5, 5);
            row4.Add(6, 6);
            row4.Add(7, 7);
            row4.Add(8, 8);
            row4.Add(9, 9);
            _matrix.Add(3, row4);
            _rowIteration = new System.Collections.ArrayList();
            _rowIteration.Add(0);
            _rowIteration.Add(3);
            _rowIteration.Add(2);
            _rowIteration.Add(1);
        }

        public Validator29(
            Int32 modulo,
            System.Collections.Hashtable matrix
            )
        {
            m_modulo = modulo;
            _matrix = matrix;
            _rowIteration = new System.Collections.ArrayList();
            _rowIteration.Add(0);
            _rowIteration.Add(3);
            _rowIteration.Add(2);
            _rowIteration.Add(1);
        }

        public Validator29(
            Int32 modulo,
            System.Collections.Hashtable matrix,
            System.Collections.ArrayList rowIteration
            ) : base(matrix, rowIteration)
        {
            m_modulo = modulo;
        }

        protected override void ProcessNumber(int transformedNumber)
        {
            m_aux += transformedNumber;
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (m_modulo - m_aux & m_modulo)%10;
            return check == getCheckNumber(accountId);
        }
    }
}