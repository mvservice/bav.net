﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator27.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator27 : ValidatorKontoCheck
    {
        public override bool IsValid(Account account)
        {
            Boolean returnValue = false;
            string ktonr = account.Number;
            try
            {
                Int64.Parse(ktonr);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            if (Int64.Parse(ktonr) >= 1 && Int64.Parse(ktonr) <= 999999999)
            {
                Int32 val1, val2, val3, val4, val5, val6, val7, val8, val9;
                val1 = val2 = val3 = val4 = val5 = val6 = val7 = val8 = val9 = 0;
                if (ktonr.Length == 9)
                    val9 = Int16.Parse(ktonr.Substring(8, 1))*2;
                if (ktonr.Length >= 8)
                    val8 = Int16.Parse(ktonr.Substring(7, 1))*1;
                if (ktonr.Length >= 7)
                    val7 = Int16.Parse(ktonr.Substring(6, 1))*2;
                if (ktonr.Length >= 6)
                    val6 = Int16.Parse(ktonr.Substring(5, 1))*1;
                if (ktonr.Length >= 5)
                    val5 = Int16.Parse(ktonr.Substring(4, 1))*2;
                if (ktonr.Length >= 4)
                    val4 = Int16.Parse(ktonr.Substring(3, 1))*1;
                if (ktonr.Length >= 3)
                    val3 = Int16.Parse(ktonr.Substring(2, 1))*2;
                if (ktonr.Length >= 2)
                    val2 = Int16.Parse(ktonr.Substring(1, 1))*1;
                if (ktonr.Length >= 1)
                    val1 = Int16.Parse(ktonr.Substring(0, 1))*2;
                Int32 ges = CrossSum(val1) + CrossSum(val2) + CrossSum(val3) + CrossSum(val4) + CrossSum(val5) +
                            CrossSum(val6) + CrossSum(val7) + CrossSum(val8) + CrossSum(val9);
                string strGes = ges.ToString();
                Int32 gesEiner = 0;
                if (strGes.Length > 1)
                {
                    for (int i = 1; i < strGes.Length; i++)
                    {
                        gesEiner = Int32.Parse(strGes.Substring(i, 1));
                    }
                }
                else
                {
                    gesEiner = Int32.Parse(strGes);
                }
                Int32 rest = 10 - gesEiner%10;
                Int32 pruef = 0;
                if (rest == 10)
                    pruef = 0;
                else
                    pruef = rest;
                try
                {
                    returnValue = pruef == Int32.Parse(ktonr.Substring(9, 1));
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    returnValue = false;
                }
            }
            else
            {
                // Transformationsmatrix (Verfahren M10H)
                Int32 summe = 0;
                Int32[] zeile1 = {0, 1, 5, 9, 3, 7, 4, 8, 2, 6};
                Int32[] zeile2 = {0, 1, 7, 6, 9, 8, 3, 2, 5, 4};
                Int32[] zeile3 = {0, 1, 8, 4, 6, 2, 9, 5, 7, 3};
                Int32[] zeile4 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
                Int32 length = ktonr.Length - 1;
                int i = 0;
                for (i = 0; i < length; i++)
                {
                    switch (i%4)
                    {
                        case 0:
                            summe += zeile1[Int32.Parse(ktonr.Substring(length - 1 - i, 1))];
                            break;
                        case 1:
                            summe += zeile2[Int32.Parse(ktonr.Substring(length - 1 - i, 1))];
                            break;
                        case 2:
                            summe += zeile3[Int32.Parse(ktonr.Substring(length - 1 - i, 1))];
                            break;
                        case 3:
                            summe += zeile4[Int32.Parse(ktonr.Substring(length - 1 - i, 1))];
                            break;
                    }
                }
                string strSumme = summe.ToString();
                Int32 summeLength = strSumme.Length;
                string summeEiner = "";
                if (summeLength > 1)
                {
                    for (int j = 1; j < summeLength; j++)
                    {
                        summeEiner = strSumme.Substring(j, 1);
                    }
                }
                else
                {
                    summeEiner = strSumme;
                }
                String pruefziffer = (10 - Int32.Parse(summeEiner)).ToString();
                returnValue = pruefziffer == ktonr.Substring(i, 1);
            }
            return returnValue;
        }
    }
}