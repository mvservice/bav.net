﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator01 : ValidatorWeightedDefault
    {
        public Validator01()
            : this(10)
        {
        }
        public Validator01(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(3);
            this._weight.Add(7);
            this._weight.Add(1);
 
        }
        public Validator01(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }
        public Validator01(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }
        public Validator01(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator01(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }
    }
}
