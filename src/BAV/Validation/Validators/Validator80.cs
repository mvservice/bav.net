﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator80.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator80 : Validator00
    {
        private readonly Validator51Exception m_exceptionValidator;
        private readonly Validator00 m_validator_00;

        public Validator80()
            : base((int) 7, (int) -2, (int) 4, new[] {new System.Collections.ArrayList(new Object[] {2, 1})})
        {
            m_exceptionValidator = new Validator51Exception();
            m_validator_00 = new Validator00((int) 10, (int) -2, (int) 4, new[] {new System.Collections.ArrayList(new Object[] {2, 1})});
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;
            if (m_exceptionValidator.isExceptional(accountID))
                return m_exceptionValidator.IsValid(account);
            return (Boolean) m_validator_00.IsValid(account) || (Boolean) base.IsValid(account);
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = m_aux%Modulo;
            if (check != 0)
                check = Modulo - check;


            return check == getCheckNumber(accountId);
        }
    }
}