﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator11.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator11 : Validator06
    {
        public Validator11()
            : this(11)
        {
        }

        public Validator11(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
            _weight.Add(8);
            _weight.Add(9);
            _weight.Add(10);
        }

        public Validator11(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator11(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator11(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator11(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, weight, start, end, checkNumberPos)
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = m_aux%Modulo;
            if (check > 1)
            {
                check = Modulo - check;
            }
            else if (check == 1)
            {
                check = 9;
            }
            else
            {
                check = 0;
            }

            return check == getCheckNumber(accountId);
        }
    }
}