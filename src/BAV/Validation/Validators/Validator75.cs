﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator75.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator75 : Validator00
    {
        public Validator75()
            : this(10)
        {
        }

        public Validator75(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(1);
        }

        public Validator75(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : base(modulo, weight)
        {
        }

        protected override bool isValid_accountID(string accountID)
        {
            string tmpAccount = LeftTrim(accountID);
            switch (tmpAccount.Length)
            {
                case 6:
                case 7:
                    m_start = 4;
                    break;
                case 9:
                    if (tmpAccount[1] == '9')
                        m_start = 2;
                    else
                        m_start = 1;
                    break;
                default:
                    return false;
            }
            m_end = m_start + 4;
            m_checkNumberPos = m_end + 1;

            return base.isValid_accountID(accountID);
        }
    }
}