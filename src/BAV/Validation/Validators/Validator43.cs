﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator43.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator43 : ValidatorWeightedDefault
    {
        public Validator43()
            : this(10)
        {
        }

        public Validator43(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
            _weight.Add(8);
            _weight.Add(9);
        }

        public Validator43(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator43(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator43(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator43(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (Modulo - m_aux%Modulo)%10;
            return check == getCheckNumber(accountId);
        }
    }
}