﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator66.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator66 : ValidatorWeightedDefault
    {
        public Validator66(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 0, 0, 7}))
        {
        }

        public Validator66() : this(11)
        {
        }

        public Validator66(Int32 modulo, System.Collections.ArrayList weight) : base(modulo, new[] {weight})
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (Modulo - m_aux%Modulo)%10;
            return check == getCheckNumber(accountId);
        }
    }
}