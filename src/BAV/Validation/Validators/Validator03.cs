﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator03.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator03 : Validator01
    {
        public Validator03()
            : this(10)
        {
        }

        public Validator03(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(1);
        }

        public Validator03(Int32 modulo, System.Collections.ArrayList weight)
            : base(modulo, weight)
        {
        }
    }
}