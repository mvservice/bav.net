﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB0.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB0 : Validator
    {
        private readonly Validator06 m_validator_06;
        private readonly Validator09 m_validator_09;

        public ValidatorB0()
        {
            m_validator_09 = new Validator09();
            m_validator_06 = new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}));
        }

        public override bool IsValid(Account account)
        {
            string accountID = account.Number.TrimStart('0');
            if (accountID.Length != 10 || accountID.Substring(0, 1) == "8")
                return false;
            switch (accountID.Substring(7, 1))
            {
                case "1":
                case "2":
                case "3":
                case "6":
                    return m_validator_09.IsValid(account);
                default:
                    return m_validator_06.IsValid(account);
            }
        }
    }
}