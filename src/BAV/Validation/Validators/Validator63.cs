﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator63 : Validator
    {
        public override bool IsValid(Account account)
        {
            string accountID = this.PreNulls(_account.Number);
            if (accountID == "")
                return false;
            if (accountID[0] != '0')
                return false;
            else if (accountID.Substring(0, 3) == "000")
                accountID = accountID.Substring(2);

            string checkNumber = accountID.Substring(1, 6);
            Int16 proofNumber = Int16.Parse(accountID.Substring(7, 1));
            Int32 crossSum = 0;

            for (int i = 5; i >= 0; i--)
            {
                crossSum += this.CrossSum(Int32.Parse(checkNumber.Substring(i, 1)) * (i % 2 + 1));
            }
            Int16 result = (Int16)((10 - Int32.Parse(crossSum.ToString().Substring(crossSum.ToString().Length - 1))) % 10);
            return result == proofNumber;
        }
    }
}
