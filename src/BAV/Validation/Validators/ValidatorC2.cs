﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorC2.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorC2 : ValidatorMulti
    {
        public ValidatorC2()
        {
            _validators.Add(new Validator22(10));
            _validators.Add(new Validator00(10));
        }
    }
}