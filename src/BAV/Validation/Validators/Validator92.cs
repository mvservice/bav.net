﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator92 : Validator01
    {
        public Validator92()
            : this(10)
        {
        }
        public Validator92(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(3);
            this._weight.Add(7);
            this._weight.Add(1);
        }
        public Validator92(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }
        public Validator92(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 3)
        {
        }
        public Validator92(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : base(modulo, weight, start, end)
        {
        }

    }
}
