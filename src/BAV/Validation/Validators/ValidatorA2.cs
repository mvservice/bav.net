﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA2.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA2 : ValidatorMulti
    {
        public ValidatorA2()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            System.Collections.ArrayList weight2 = new System.Collections.ArrayList();
            weight1.Add(2);
            weight1.Add(1);
            weight2.Add(2);
            weight2.Add(3);
            weight2.Add(4);
            weight2.Add(5);
            weight2.Add(6);
            weight2.Add(7);
            _validators.Add(new Validator00(10, weight1));
            _validators.Add(new Validator04(11, weight2));
        }
    }
}