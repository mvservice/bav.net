﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA7.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA7 : ValidatorMulti
    {
        public ValidatorA7()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            weight1.Add(2);
            weight1.Add(1);

            _validators.Add(new Validator00(10, weight1));
            _validators.Add(new Validator03());
        }
    }
}