﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator81.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator81 : Validator
    {
        private readonly Validator51Exception m_exceptionValidator;
        private readonly Validator32 m_validator_32;

        public Validator81()
        {
            m_exceptionValidator = new Validator51Exception();
            m_validator_32 = new Validator32(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}));
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;

            if (m_exceptionValidator.isExceptional(accountID))
                return m_exceptionValidator.IsValid(account);
            return m_validator_32.IsValid(account);
        }
    }
}