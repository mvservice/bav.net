﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA4.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA4 : ValidatorMulti
    {
        private readonly System.Collections.ArrayList m_defaultValidators = new System.Collections.ArrayList();
        private readonly System.Collections.ArrayList m_ExceptionValidators = new System.Collections.ArrayList();

        public ValidatorA4()
        {
            m_defaultValidators.Add(new Validator06(11,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 0, 0, 0}), -2, 3));
            m_defaultValidators.Add(new Validator06(7,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 0, 0, 0}), -2, 3));
            m_ExceptionValidators.Add(new Validator06(11,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}), -2, 4));
            Validator93 methode4 = new Validator93(11, 7, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6}),
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6}));

            m_defaultValidators.Add(methode4);
            m_ExceptionValidators.Add(methode4);
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;

            if (accountID.Substring(2, 2) != "99")
                _validators = m_defaultValidators;
            else
                _validators = m_ExceptionValidators;
            return base.IsValid(account);
        }
    }
}