﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB2.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB2 : Validator
    {
        private readonly Validator00 m_validator_00;
        private readonly Validator02 m_validator_02;

        public ValidatorB2()
        {
            m_validator_00 = new Validator00(10, new System.Collections.ArrayList(new Object[] {2, 1}));
            m_validator_02 = new Validator02(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 8, 9}));
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;


            return (Int32.Parse(accountID.Substring(0, 1)) <= 7)
                ? m_validator_02.IsValid(account)
                : m_validator_00.IsValid(account);
        }
    }
}