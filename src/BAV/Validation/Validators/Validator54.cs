﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator54.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator54 : ValidatorWeightedDefault
    {
        public Validator54()
            : this(11)
        {
        }

        public Validator54(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
        }

        public Validator54(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator54(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 2)
        {
        }

        public Validator54(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : base(modulo, start, end, new[] {weight})
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = Modulo - m_aux%Modulo;
            return check < 10 && check == getCheckNumber(accountId);
        }

        public override bool IsValid(Account account)
        {
            string accountID = account.Number;
            if (accountID.Length != 10 || accountID.Substring(0, 2) != "49")
                return false;
            return base.IsValid(account);
        }
    }
}