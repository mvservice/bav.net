﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator07 : Validator02
    {
        public Validator07()
            : this(11)
        {
        }
        public Validator07(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(2);
            this._weight.Add(3);
            this._weight.Add(4);
            this._weight.Add(5);
            this._weight.Add(6);
            this._weight.Add(7);
            this._weight.Add(8);
            this._weight.Add(9);
            this._weight.Add(10);
 
        }
        public Validator07(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : base(modulo, weight)
        {
        }
    }
}
