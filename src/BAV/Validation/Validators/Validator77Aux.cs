﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator77Aux : ValidatorWeightedDefault
    {
        private Int32 m_checknumber;
        public Validator77Aux()
            : this(11)
        {
        }
        public Validator77Aux(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(1);
            this._weight.Add(2);
            this._weight.Add(3);
            this._weight.Add(4);
            this._weight.Add(5);
        }
        public Validator77Aux(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -1)
        {
        }
        public Validator77Aux(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 5)
        {
        }
        public Validator77Aux(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, 0)
        {
        }

        public Validator77Aux(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
            this.m_checknumber = checkNumberPos;
        }
        protected override bool IsValid(string accountId)
        {
            Int32 check = this.m_aux % this.Modulo;
            return check == this.getCheckNumber(accountId);
        }
    }
}
