﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator96 : Validator
    {
        private Validator00 m_validator_00;
        private Validator19 m_validator_19;
        private Int64 m_from;
        private Int64 m_to;

        public Validator96(Int64 from, Int64 to)
        {
            this.m_from = from;
            this.m_to = to;

            this.m_validator_00 = new Validator00(10, new System.Collections.ArrayList(new Object[] { 2, 1 }));
            this.m_validator_19 = new Validator19(11, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6, 7, 8, 9, 1 }));
        }

        public Validator96(long from) : this(from,9399999)
        {  
        }

        public Validator96() : this(1300000)
        {    
        }

        

        public override bool IsValid(Account account)
        {
            return (Boolean)this.m_validator_19.IsValid(account)
             || (Boolean)this.m_validator_00.IsValid(account)
             || (System.Int64.Parse(account.Number) >= this.m_from && System.Int64.Parse(account.Number) <= this.m_to);
        }
    }
}
