﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator77.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator77 : ValidatorMulti
    {
        public Validator77()
        {
            _validators.Add(new Validator77Aux(11, new System.Collections.ArrayList(new Object[] {1, 2, 3, 4, 5})));
            _validators.Add(new Validator77Aux(11, new System.Collections.ArrayList(new Object[] {5, 4, 3, 4})));
        }
    }
}