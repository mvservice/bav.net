﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator13.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator13 : ValidatorKontoCheck
    {
        public override bool IsValid(Account account)
        {
            Int32 val7 = (Convert.ToInt32(account.Number.Substring(6, 1))*2);
            Int32 val6 = (Convert.ToInt32(account.Number.Substring(5, 1))*1);
            Int32 val5 = (Convert.ToInt32(account.Number.Substring(4, 1))*2);
            Int32 val4 = (Convert.ToInt32(account.Number.Substring(3, 1))*1);
            Int32 val3 = (Convert.ToInt32(account.Number.Substring(2, 1))*2);
            Int32 val2 = (Convert.ToInt32(account.Number.Substring(1, 1))*1);

            if (val2 > 9)
                val2 = Convert.ToInt32(val2.ToString().Substring(0, 1)) +
                       Convert.ToInt32(val2.ToString().Substring(1, 1));
            if (val3 > 9)
                val3 = Convert.ToInt32(val3.ToString().Substring(0, 1)) +
                       Convert.ToInt32(val3.ToString().Substring(1, 1));
            if (val4 > 9)
                val4 = Convert.ToInt32(val4.ToString().Substring(0, 1)) +
                       Convert.ToInt32(val4.ToString().Substring(1, 1));
            if (val5 > 9)
                val5 = Convert.ToInt32(val5.ToString().Substring(0, 1)) +
                       Convert.ToInt32(val5.ToString().Substring(1, 1));
            if (val6 > 9)
                val6 = Convert.ToInt32(val6.ToString().Substring(0, 1)) +
                       Convert.ToInt32(val6.ToString().Substring(1, 1));
            if (val7 > 9)
                val7 = Convert.ToInt32(val7.ToString().Substring(0, 1)) +
                       Convert.ToInt32(val7.ToString().Substring(1, 1));

            Int32 ges = val2 + val3 + val4 + val5 + val6 + val7;
            String sGes = ges.ToString();

            Int32 pruef = 0;
            if (sGes.Length > 1)
            {
                pruef = 10 - Convert.ToInt32(sGes[1].ToString());
                // Wenn 10 - Pruefziffer == 0, setze $pruef = 0
                if (pruef == 10)
                    pruef = 0;
            }
            else
            {
                pruef = 10 - ges;
                // Wenn 10 - Pruefziffer == 0, setze $pruef = 0
                if (pruef == 10)
                    pruef = 0;
            }

            if (pruef == Convert.ToInt32(account.Number.Substring(7, 1)))
                return true;
            if (Convert.ToInt32(account.Number[8]) + Convert.ToInt32(account.Number[9]) != 0)
            {
                Int32[] cache = new Int32[10];
                cache[0] = Convert.ToInt32(account.Number[2]);
                cache[1] = Convert.ToInt32(account.Number[3]);
                cache[2] = Convert.ToInt32(account.Number[4]);
                cache[3] = Convert.ToInt32(account.Number[5]);
                cache[4] = Convert.ToInt32(account.Number[6]);
                cache[5] = Convert.ToInt32(account.Number[7]);
                cache[6] = Convert.ToInt32(account.Number[8]);
                cache[7] = Convert.ToInt32(account.Number[9]);
                cache[8] = 0;
                cache[9] = 0;
                String ktonr = "";

                for (int i = 0; i <= 9; i++)
                {
                    ktonr += cache[i].ToString();
                }
                Account acc = new Account(ktonr, account.Bank);

                return IsValid(acc);
            }
            return false;
        }
    }
}