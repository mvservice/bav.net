﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator30.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator30 : Validator00
    {
        public Validator30()
            : this(10)
        {
        }

        public Validator30(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(1);
            _weight.Add(0);
            _weight.Add(0);
            _weight.Add(0);
            _weight.Add(0);
            _weight.Add(2);
        }

        public Validator30(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator30(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator30(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator30(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, new[] {weight})
        {
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += (number*weight);
        }
    }
}