﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator52.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator52 : ValidatorWeightedDefault
    {
        private readonly Validator20 m_exceptionValidator;

        public Validator52()
            : this(11)
        {
        }

        public Validator52(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(4);
            _weight.Add(8);
            _weight.Add(5);
            _weight.Add(10);
            _weight.Add(9);
            _weight.Add(7);
            _weight.Add(3);
            _weight.Add(6);
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(4);
        }

        public Validator52(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -1)
        {
        }

        public Validator52(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator52(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, 1)
        {
        }

        public Validator52(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
            m_exceptionValidator = new Validator20();
        }

        protected override bool isValid_accountID(string accountID)
        {
            Int32 accountIDlength = LeftTrim(accountID).Length;
            if (accountIDlength == 10 && accountID[0] == '9')
                return (Boolean) m_exceptionValidator.IsValid(_account);

            string eser = getESER();
            if (eser == "")
                return false;

            return base.isValid_accountID(eser);
        }

        protected override bool IsValid(string accountId)
        {
            Int32 checkWeight = getCheckWeight(accountId);
            Int32 check = getCheckNumber(_account.Number);
            return 10 == (m_aux + checkWeight*check)%Modulo;
        }

        protected override int getCheckNumber(string accountID)
        {
            return Int32.Parse(accountID.Substring(m_checkNumberPos, 1));
        }

        protected virtual Int32 getCheckWeight(string eser)
        {
            return (Int32) _weight[(eser.Length - 6)%_weight.Count];
        }

        protected virtual string getESER()
        {
            string bankID = _account.Bank.Blz;
            string accountID = _account.Number;
            if (accountID.Length != 8)
                return "";
            if (bankID[3] != '5')
                return "";

            string blzPart = LeftTrim(bankID.Substring(4));
            if (blzPart == "")
                return "";

            string accountPart = LeftTrim(accountID.Substring(2));
            return blzPart + accountID[0] + "0" + accountPart;
        }
    }
}