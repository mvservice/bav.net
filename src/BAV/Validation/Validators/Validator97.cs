﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator97.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator97 : Validator
    {
        protected Int32 m_modulo;

        public Validator97()
            : this(11)
        {
        }

        public Validator97(Int32 modulo)
        {
            m_modulo = modulo;
        }

        public override bool IsValid(Account account)
        {
            string accountID = account.Number.TrimStart('0');
            if (accountID.Length < 5)
                return false;

            string x = accountID.Substring(0, accountID.Length - 2);
            Int64 y = (Int64.Parse(x)/m_modulo)*m_modulo;
            string check = ((Int64.Parse(x) - y)%10).ToString();
            return check == accountID.Substring(accountID.Length - 1, 1);
        }
    }
}