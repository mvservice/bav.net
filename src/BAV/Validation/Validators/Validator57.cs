﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator57.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator57 : ValidatorWeighted
    {
        public Validator57()
            : this(10)
        {
        }

        public Validator57(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(1);
            _weight.Add(2);
        }

        public Validator57(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : base(modulo, (int) 0, (int) -2, new[] {weight})
        {
        }

        protected override bool isValid_accountID(string accountID)
        {
            Int32 firstTwo = Int32.Parse(accountID.Substring(0, 2));
            if ((firstTwo >= 0 && firstTwo <= 50)
                || firstTwo == 91 || firstTwo >= 96)
                return true;

            Boolean is7or8 = true;
            for (int i = 0; i < 7; i++)
            {
                if (accountID[i] != '7' && accountID[i] != '8')
                {
                    is7or8 = false;
                    break;
                }
            }

            if (is7or8)
                return true;

            return base.isValid_accountID(accountID);
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += CrossSum(number*weight);
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (Modulo - Int32.Parse(m_aux.ToString().Substring(m_aux.ToString().Length - 1)))%10;
            return check == getCheckNumber(accountId);
        }
    }
}