﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator53.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator53 : Validator52
    {
        public Validator53()
            : this(11)
        {
        }

        public Validator53(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(4);
            _weight.Add(8);
            _weight.Add(5);
            _weight.Add(10);
            _weight.Add(9);
            _weight.Add(7);
            _weight.Add(3);
            _weight.Add(6);
            _weight.Add(1);
        }

        public Validator53(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -1)
        {
        }

        public Validator53(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator53(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, 2)
        {
        }

        public Validator53(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, weight, start, end, checkNumberPos)
        {
        }

        protected override string getESER()
        {
            string bankID = _account.Bank.Blz;
            string accountID = _account.Number;
            if (accountID.Length != 9)
                return "";
            if (bankID[3] != '5')
                return "";

            string blzPart0 = bankID.Substring(bankID.Length - 4, 2);
            string blzPart1 = bankID.Substring(bankID.Length - 1);
            string accountTail = LeftTrim(accountID.Substring(3));
            Char accountPart0 = accountID[0];
            Char t = accountID[1];
            Char p = accountID[2];

            return blzPart0 + t + blzPart1 + accountPart0 + "0" + accountTail;
        }
    }
}