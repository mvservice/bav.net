﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB5.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB5 : Validator
    {
        private readonly Validator00 m_validator_00;
        private readonly Validator01 m_validator_01;

        public ValidatorB5()
        {
            m_validator_00 = new Validator00(10, new System.Collections.ArrayList(new Object[] {2, 1}));
            m_validator_01 = new Validator01(10, new System.Collections.ArrayList(new Object[] {7, 3, 1}));
        }

        public override bool IsValid(Account account)
        {
            if ((Boolean) m_validator_01.IsValid(account))
                return true;
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;
            return (accountID.Substring(0, 1) == "9" || accountID.Substring(0, 1) == "8")
                ? false
                : m_validator_00.IsValid(account);
        }
    }
}