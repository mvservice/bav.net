﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator91.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator91 : ValidatorMulti
    {
        public Validator91()
        {
            _validators.Add(new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}), 5, 0,
                6));
            _validators.Add(new Validator06(11, new System.Collections.ArrayList(new Object[] {7, 6, 5, 4, 3, 2}), 5, 0,
                6));
            _validators.Add(new Validator06(11,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 0, 5, 6, 7, 8, 9, 10}), -1, 0, 6));
            _validators.Add(new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 4, 8, 5, 10, 9}), 5,
                0, 6));
        }
    }
}