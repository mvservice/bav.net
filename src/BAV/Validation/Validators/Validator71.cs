﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator71.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator71 : Validator06
    {
        public Validator71()
            : this(11)
        {
        }

        public Validator71(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.AddRange(new Object[] {6, 5, 4, 3, 2, 1});
        }

        public Validator71(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, 1)
        {
        }

        public Validator71(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 6)
        {
        }

        public Validator71(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : base(modulo, weight, start, end)
        {
        }
    }
}