﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator93 : ValidatorMulti
    {
        public Validator93(Int32 modulo1,Int32 modulo2,System.Collections.ArrayList weight1,System.Collections.ArrayList weight2)
        {
            this._validators.Add(new Validator93Aux(modulo1, weight1));
            this._validators.Add(new Validator93Aux(modulo2, weight2));
        }

        public Validator93() : this(11)
        {
        }

        public Validator93(Int32 modulo1)
            : this(modulo1,7)
        {
        }
        public Validator93(Int32 modulo1,Int32 modulo2)
            : this(modulo1, modulo2,new System.Collections.ArrayList(new Object[]{2,3,4,5,6}))
        {
        }
        public Validator93(Int32 modulo1, Int32 modulo2,System.Collections.ArrayList weight1)
            : this(modulo1, modulo2, weight1, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 }))
        {
        }
    }
}
