﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator17.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator17 : ValidatorWeightedDefault
    {
        public Validator17()
            : base(11, 1, 6, 7, new System.Collections.ArrayList(new Object[] {1, 2}))
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (m_aux - 1)%Modulo;
            return check == getCheckNumber(accountId);
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += CrossSum(number*weight);
        }
    }
}