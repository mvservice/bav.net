﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB1.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB1 : ValidatorMulti
    {
        public ValidatorB1()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            System.Collections.ArrayList weight2 = new System.Collections.ArrayList();
            weight1.Add(7);
            weight1.Add(3);
            weight1.Add(1);
            weight2.Add(3);
            weight2.Add(7);
            weight2.Add(1);
            _validators.Add(new Validator05(10, weight1));
            _validators.Add(new Validator01(10, weight2));
        }
    }
}