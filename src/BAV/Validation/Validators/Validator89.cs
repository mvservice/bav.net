﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator89.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator89 : Validator06
    {
        private readonly Validator10 m_validator_10;

        public Validator89()
            : this(11)
        {
        }

        public Validator89(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
        }

        public Validator89(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator89(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 3)
        {
        }

        public Validator89(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : base(modulo, weight, start, end)
        {
            m_validator_10 = new Validator10();
        }

        public override bool IsValid(Account account)
        {
            string accountID = LeftTrim(account);
            switch (accountID.Length)
            {
                case 7:
                    return base.IsValid(account);
                case 8:
                case 9:
                    return m_validator_10.IsValid(account);
                default:
                    return true;
            }
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += CrossSum(number*weight);
        }
    }
}