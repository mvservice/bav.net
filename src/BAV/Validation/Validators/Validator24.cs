﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator24.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator24 : ValidatorWeighted
    {
        public Validator24()
            : this(11)
        {
        }

        public Validator24(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(3);
        }

        public Validator24(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, 0)
        {
        }

        public Validator24(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, -2)
        {
        }

        public Validator24(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator24(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, new[] {weight})
        {
        }

        protected override bool isValid_accountID(string accountID)
        {
            Int32 start = m_start;
            switch (accountID.Substring(start, 1))
            {
                case "3":
                case "4":
                case "5":
                case "6":
                    start = 1;
                    break;
                case "9":
                    if (accountID.Substring(start + 3, 1) == "0")
                        return false;
                    start = 3;
                    break;
                default:
                    start = 0;
                    break;
            }
            for (; start < accountID.Length && accountID.Substring(start, 1) == "0"; start++)
            {
                if (start >= accountID.Length)
                    return false;
            }
            Int32 originalStart = m_start;
            m_start = start;
            Boolean result = base.isValid_accountID(accountID);
            m_start = originalStart;
            return result;
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = Int32.Parse(m_aux.ToString().Substring(m_aux.ToString().Length - 1));
            return check == getCheckNumber(accountId);
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += (number*weight + weight) & Modulo;
        }
    }
}