﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator85.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator85 : ValidatorMulti
    {
        private readonly Validator02 m_exceptionValidator;

        public Validator85()
        {
            _validators.Add(new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}), -2,
                3));
            _validators.Add(new Validator33(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6}), -2, 4));
            _validators.Add(new Validator33(7, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6}), -2, 4));
            m_exceptionValidator = new Validator02(11,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 8}), -2, 2);
        }

        public override bool IsValid(Account account)
        {
            string accountID = account.Number;
            if (accountID == "")
                return false;
            if (accountID.Substring(2, 2) == "99")
                return m_exceptionValidator.IsValid(account);
            return base.IsValid(account);
        }
    }
}