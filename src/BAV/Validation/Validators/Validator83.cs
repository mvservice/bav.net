﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator83.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator83 : ValidatorMulti
    {
        private readonly Validator06 m_sachValidator;

        public Validator83()
        {
            _validators.Add(new Validator32(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7})));
            _validators.Add(new Validator33(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6})));
            _validators.Add(new Validator33(7, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6})));

            m_sachValidator = new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 8}),
                -2, 2);
        }

        public override bool IsValid(Account account)
        {
            string accountID = account.Number;
            if (accountID == "")
                return false;

            if (accountID.Substring(2, 2) == "99")
                return m_sachValidator.IsValid(account);
            return base.IsValid(account);
        }

        protected override bool ContinueCondition(Account account)
        {
            switch (_current)
            {
                case 1:
                    string check = account.Number.Substring(account.Number.Length - 1);
                    if (Int32.Parse(check) >= 7)
                    {
                        return false;
                    }
                    break;
            }
            return true;
        }
    }
}