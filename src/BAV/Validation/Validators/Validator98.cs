﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator98.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator98 : Validator01
    {
        public Validator98()
            : this(10)
        {
        }

        public Validator98(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(3);
            _weight.Add(1);
            _weight.Add(7);
        }

        public Validator98(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator98(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 2)
        {
        }

        public Validator98(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : base(modulo, weight, start, end)
        {
        }
    }
}