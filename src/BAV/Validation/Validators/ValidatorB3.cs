﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB3.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB3 : Validator
    {
        private readonly Validator06 m_validator_06;
        private readonly Validator32 m_validator_32;

        public ValidatorB3()
        {
            m_validator_32 = new Validator32(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}));
            m_validator_06 = new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7}));
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;


            return (accountID.Substring(0, 1) == "9")
                ? m_validator_06.IsValid(account)
                : m_validator_32.IsValid(account);
        }
    }
}