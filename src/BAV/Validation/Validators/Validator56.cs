﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator56.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator56 : ValidatorWeightedDefault
    {
        public Validator56()
            : this(11)
        {
        }

        public Validator56(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
        }

        public Validator56(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : base(modulo, new[] {weight})
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = Modulo - m_aux%Modulo;
            if (accountId.Substring(0, 1) == "9")
            {
                if (check == 10)
                    check = 7;
                else if (check == 11)
                    check = 8;
            }
            return check == getCheckNumber(accountId);
        }
    }
}