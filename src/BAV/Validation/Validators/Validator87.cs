﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator87.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator87 : ValidatorMulti
    {
        private readonly Validator51Exception m_exceptionValidator;

        public Validator87()
        {
            _validators.Add(new Validator33(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6}), -2, 4));
            _validators.Add(new Validator06(7, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6}), -2, 4));
            m_exceptionValidator = new Validator51Exception();
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;

            if (m_exceptionValidator.isExceptional(accountID))
                return m_exceptionValidator.IsValid(account);
            if (uglyAlgorithm(accountID))
                return true;

            return base.IsValid(account);
        }

        private Boolean uglyAlgorithm(string accountID)
        {
            int i = 0;
            int c2 = 0;
            int d2 = 0;
            int a5 = 0;
            Int32 p = 0;
            Int32[] tab1 = {0, 4, 3, 2, 6};
            Int32[] tab2 = {7, 1, 5, 9, 8};
            System.Collections.ArrayList konto = new System.Collections.ArrayList();
            konto.Add(null);
            for (i = 0; i < accountID.Length; i++)
            {
                konto.Add(accountID.Substring(i, 1));
            }
            i = 4;
            while (i < konto.Count && (string) konto[i] == "0")
            {
                i++;
            }
            c2 = i%2;

            while (i < 10)
            {
                switch ((string) konto[i])
                {
                    case "0":
                        konto[i] = "5";
                        break;
                    case "1":
                        konto[i] = "6";
                        break;
                    case "5":
                        konto[i] = "10";
                        break;
                    case "6":
                        konto[i] = "1";
                        break;
                }
                if (c2 == d2)
                {
                    if (Int32.Parse((string) konto[i]) > 5)
                    {
                        if (c2 == 0 && d2 == 0)
                        {
                            c2 = 1;
                            d2 = 1;
                            a5 = a5 + 6 - (Int32.Parse((string) konto[i]) - 6);
                        }
                        else
                        {
                            c2 = 0;
                            d2 = 0;
                            a5 = a5 + Int32.Parse((string) konto[i]);
                        }
                    }
                    else
                    {
                        if (c2 == 0 && d2 == 0)
                        {
                            c2 = 1;
                            a5 = a5 + Int32.Parse((string) konto[i]);
                        }
                        else
                        {
                            c2 = 0;
                            a5 = a5 + Int32.Parse((string) konto[i]);
                        }
                    }
                }
                else
                {
                    if (Int32.Parse((string) konto[i]) > 5)
                    {
                        if (c2 == 0)
                        {
                            c2 = 1;
                            d2 = 0;
                            a5 = a5 - 6 + (Int32.Parse((string) konto[i]) - 6);
                        }
                        else
                        {
                            c2 = 0;
                            d2 = 1;
                            a5 = a5 - Int32.Parse((string) konto[i]);
                        }
                    }
                    else
                    {
                        if (c2 == 0)
                        {
                            c2 = 1;
                            a5 = a5 - Int32.Parse((string) konto[i]);
                        }
                        else
                        {
                            c2 = 0;
                            a5 = a5 - Int32.Parse((string) konto[i]);
                        }
                    }
                }
                i++;
            }

            while (a5 < 0 || a5 > 4)
            {
                if (a5 > 4)
                {
                    a5 = a5 - 5;
                }
                else
                {
                    a5 = a5 + 5;
                }
            }
            if (d2 == 0)
            {
                p = tab1[a5];
            }
            else
            {
                p = tab2[a5];
            }
            if (p == Int32.Parse((string) konto[10]))
                return true;
            if ((string) konto[4] == "0")
            {
                if (p > 4)
                {
                    p = p - 5;
                }
                else
                {
                    p = p + 5;
                }
                if (p == Int32.Parse((string) konto[10]))
                    return true;
            }

            return false;
        }
    }
}