﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA5.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA5 : ValidatorMulti
    {
        public ValidatorA5()
        {
            _validators.Add(new Validator00(10, new System.Collections.ArrayList(new Object[] {2, 1})));
            _validators.Add(new Validator10(11,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 8, 9, 10})));
        }

        protected override bool ContinueCondition(Account account)
        {
            string accountID = account.Number;
            return !(accountID.Length == 10 && accountID.Substring(0, 1) == "9");
        }
    }
}