﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA6.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA6 : Validator
    {
        private readonly Validator00 m_validator_00;
        private readonly Validator01 m_validator_01;

        public ValidatorA6()
        {
            m_validator_00 = new Validator00(10, new System.Collections.ArrayList(new Object[] {2, 1}));
            m_validator_01 = new Validator01(10, new System.Collections.ArrayList(new Object[] {3, 7, 1}));
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;


            return (accountID.Substring(1, 1) == "8")
                ? m_validator_00.IsValid(account)
                : m_validator_01.IsValid(account);
        }
    }
}