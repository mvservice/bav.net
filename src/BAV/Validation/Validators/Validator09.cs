﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator09.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator09 : Validator
    {
        public override bool IsValid(Account account)
        {
            return true;
        }
    }
}