﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB6.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB6 : Validator
    {
        private readonly Validator20 m_validator_20;
        private readonly Validator53 m_validator_53;

        public ValidatorB6()
        {
            m_validator_20 = new Validator20(11,
                new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7, 8, 9, 3}));
            m_validator_53 = new Validator53(11,
                new System.Collections.ArrayList(new Object[] {2, 4, 8, 10, 9, 7, 3, 6, 1}));
        }

        internal override ValidatorResult GetValidatorResult(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return new ValidatorResult(Validator_Result.Invalid, account);
            if (accountID.Substring(0, 1) != "0")
                return m_validator_20.GetValidatorResult(account);
            return m_validator_53.GetValidatorResult(account);
        }

        public override bool IsValid(Account account)
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }
    }
}