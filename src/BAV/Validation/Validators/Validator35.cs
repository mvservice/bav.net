﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator35 : Validator06
    {
        public Validator35()
            : this(11)
        {
        }
        public Validator35(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(2);
            this._weight.Add(3);
            this._weight.Add(4);
            this._weight.Add(5);
            this._weight.Add(6);
            this._weight.Add(7);
            this._weight.Add(8);
            this._weight.Add(9);
            this._weight.Add(10);
            
        }
        public Validator35(Int32 modulo, System.Collections.ArrayList weight)
            : base(modulo, weight)
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = this.m_aux % this.Modulo;
            if (check >= 10)
                return accountId.Substring(this.normalizePosition(-1, accountId)) == accountId.Substring(this.normalizePosition(-2, accountId), 1);
            return check == getCheckNumber(accountId);
        }
    }
}
