﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA9.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA9 : ValidatorMulti
    {
        public ValidatorA9()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            System.Collections.ArrayList weight2 = new System.Collections.ArrayList();
            weight1.Add(3);
            weight1.Add(7);
            weight1.Add(1);
            weight2.Add(2);
            weight2.Add(3);
            weight2.Add(4);
            weight2.Add(5);
            weight2.Add(6);
            weight2.Add(7);
            _validators.Add(new Validator01(10, weight1));
            _validators.Add(new Validator06(11, weight2));
        }
    }
}