﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator39 : Validator06
    {
        public Validator39()
            : this(11)
        {
        }
        public Validator39(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(2);
            this._weight.Add(4);
            this._weight.Add(8);
            this._weight.Add(5);
            this._weight.Add(10);
            this._weight.Add(9);
            this._weight.Add(7);
        }
        public Validator39(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }
        public Validator39(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 2)
        {
        }
        public Validator39(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : base(modulo, weight, start, end)
        {
        }
    }
}
