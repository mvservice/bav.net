﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator61.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator61 : ValidatorWeighted
    {
        public Validator61()
            : this(10)
        {
        }

        public Validator61(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(1);
        }

        public Validator61(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, 0)
        {
        }

        public Validator61(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, -4)
        {
        }

        public Validator61(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -3)
        {
        }

        public Validator61(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, new[] {weight})
        {
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += CrossSum(number*weight);
        }

        protected override bool IsValid(string accountId)
        {
            if (accountId.Length >= 8 && accountId[8] == '8')
            {
                for (int i = 8; i < accountId.Length; i++)
                {
                    ProcessNumber(Int32.Parse(accountId.Substring(i, 1)), (Int32) _weight[(i - 1)%_weight.Count]);
                }
            }
            Int32 check = (Modulo - Int32.Parse(m_aux.ToString().Substring(m_aux.ToString().Length - 1)))%10;
            return check == getCheckNumber(accountId);
        }
    }
}