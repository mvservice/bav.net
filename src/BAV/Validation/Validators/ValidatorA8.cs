﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA8.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA8 : ValidatorMulti
    {
        private readonly Validator51Exception m_exceptionValidator;

        public ValidatorA8()
        {
            _validators.Add(new Validator06(11, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 7})));
            _validators.Add(new Validator00(10, new System.Collections.ArrayList(new Object[] {2, 1})));
            m_exceptionValidator = Validator51.getExceptionValidatorInstance();
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;
            if (accountID.Substring(2, 1) == "9")
                return m_exceptionValidator.IsValid(account);
            return base.IsValid(account);
        }
    }
}