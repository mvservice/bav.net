﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator84 : ValidatorMulti
    {
        private Validator51Exception m_exceptionValidator;

        public Validator84()
        {
            this._validators.Add(new Validator33(11, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 })));
            this._validators.Add(new Validator06(7, new System.Collections.ArrayList(new Object[] { 2, 3, 4, 5, 6 }), -2, 4));

            this.m_exceptionValidator = new Validator51Exception();
        }

        public override bool IsValid(Account account)
        {
            string accountID = this.PreNulls(account);
            if (accountID == "")
                return false;
            if (m_exceptionValidator.isExceptional(accountID))
                return this.m_exceptionValidator.IsValid(account);
            return base.IsValid(account);
        }
    }
}
