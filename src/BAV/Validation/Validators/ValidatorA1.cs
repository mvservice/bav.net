﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA1.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA1 : Validator00
    {
        public ValidatorA1()
            : this(10)
        {
        }

        public ValidatorA1(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(1);
            _weight.Add(2);
            _weight.Add(0);
            _weight.Add(0);
        }

        public ValidatorA1(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public ValidatorA1(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public ValidatorA1(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public ValidatorA1(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, new[] {weight})
        {
        }

        public override bool IsValid(Account account)
        {
            Int32 accountIDLength = account.Number.TrimStart('0').Length;
            if (accountIDLength == 9 || accountIDLength < 8)
                return false;
            return base.IsValid(account);
        }
    }
}