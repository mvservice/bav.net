﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator73.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator73 : ValidatorMulti
    {
        private readonly Validator51Exception m_exceptionValidator;

        public Validator73()
        {
            _validators.Add(new Validator00((int) 10, (int) -2, (int) 3, new[] {new System.Collections.ArrayList(new Object[] {2, 1})}));
            _validators.Add(new Validator00((int) 10, (int) -2, (int) 4, new[] {new System.Collections.ArrayList(new Object[] {2, 1})}));
            _validators.Add(new Validator00((int) 7, (int) -2, (int) 4, new[] {new System.Collections.ArrayList(new Object[] {2, 1})}));

            m_exceptionValidator = new Validator51Exception();
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;
            if (m_exceptionValidator.isExceptional(accountID))
                return m_exceptionValidator.IsValid(account);
            return base.IsValid(account);
        }
    }
}