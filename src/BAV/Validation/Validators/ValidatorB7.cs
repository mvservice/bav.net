﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB7 : ValidatorKontoCheck
    {
        private Validator01 m_validator_01;
        private Validator09 m_validator_09;
        public ValidatorB7()
        {
            this.m_validator_01 = new Validator01(10, new System.Collections.ArrayList(new Object[] { 3, 7, 1 }));
            this.m_validator_09 = new Validator09();

        }
       
        public override bool IsValid(Account account)
        {
            Int64 accountID = 0;
            try
            {
               accountID  = Int64.Parse(account.Number);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            if (accountID >= 100000 && accountID <= 5999999
                || accountID >= 700000000 && accountID <= 899999999)
                return this.m_validator_01.IsValid(account);
            else
                return this.m_validator_09.IsValid(account);
        }
    }
}
