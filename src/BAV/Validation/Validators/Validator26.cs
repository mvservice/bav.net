﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator26.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator26 : Validator06
    {
        public Validator26()
            : this(11)
        {
        }

        public Validator26(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
        }

        public Validator26(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -4)
        {
        }

        public Validator26(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator26(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -3)
        {
        }

        public Validator26(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, weight, start, end, checkNumberPos)
        {
        }

        protected override Boolean isValid_accountID(string accountID)
        {
            if (accountID.Substring(0, 2) == "00")
                accountID = accountID.Substring(2) + "00";
            return base.isValid_accountID(accountID);
        }
    }
}