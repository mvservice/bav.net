﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator64.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator64 : Validator06
    {
        public Validator64()
            : this(11)
        {
        }

        public Validator64(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(9);
            _weight.Add(10);
            _weight.Add(5);
            _weight.Add(8);
            _weight.Add(4);
            _weight.Add(2);
        }

        public Validator64(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, 0)
        {
        }

        public Validator64(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 5)
        {
        }

        public Validator64(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, 6)
        {
        }

        public Validator64(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, weight, start, end, checkNumberPos)
        {
        }
    }
}