﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator00.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator00 : ValidatorWeighted
    {
        public Validator00()
            : this(10)
        {
        }

        public Validator00(Int32 modulo)
            : this(modulo, (byte)2, (byte)1)
        {
        }

        public Validator00(int modulo, params byte[] weight)
            : this(modulo, -2, weight)
        {
        }

        public Validator00(int modulo, int start, params byte[] weight)
            : this(modulo, start, 0, weight)
        {
        }

        public Validator00(int modulo, int start, int end, params byte[] weight)
            : this(modulo, start, end, -1, weight)
        {
        }

        public Validator00(int modulo, int start, int end, int checkNumberPos, params byte[] weight)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }

        protected override void ProcessNumber(int number, int weight)
        {
            m_aux += CrossSum(number * weight);
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (Modulo - m_aux % Modulo) % 10;
            return check == getCheckNumber(accountId);
        }
    }
}