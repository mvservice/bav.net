﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator_51.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator51 : Validator06
    {
        private readonly Validator51Exception m_exceptionValidator;
        private readonly Validator06 m_validator_06a;
        private readonly Validator33 m_validator_33b;

        public Validator51()
        {
            _modulo = 7;
            m_start = -2;
            m_end = 4;
            _weight = new System.Collections.ArrayList();
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            System.Collections.ArrayList weight2 = new System.Collections.ArrayList();
            weight1.Add(2);
            weight1.Add(3);
            weight1.Add(4);
            weight1.Add(5);
            weight1.Add(6);
            weight1.Add(7);
            weight2.Add(2);
            weight2.Add(3);
            weight2.Add(4);
            weight2.Add(5);
            weight2.Add(6);
            m_validator_06a = new Validator06(11, weight1, -2, -3);
            m_validator_33b = new Validator33(11, weight2, -2, -4);

            m_exceptionValidator = getExceptionValidatorInstance();
        }

        public static Validator51Exception getExceptionValidatorInstance()
        {
            return new Validator51Exception();
        }

        public override bool IsValid(Account account)
        {
            String accountID = PreNulls(account);
            if (accountID == "")
                return false;
            if (m_exceptionValidator.isExceptional(accountID))
                return m_exceptionValidator.IsValid(account);

            if ((Boolean) m_validator_06a.IsValid(account) || (Boolean) m_validator_33b.IsValid(account))
                return true;

            accountID = account.Number;
            if (Int32.Parse(accountID.Substring((accountID.Length - 1))) >= 7)
                return false;

            return base.IsValid(account);
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = (Modulo - m_aux%Modulo);
            if (check == Modulo)
                check = 0;
            return check == getCheckNumber(accountId);
        }
    }
}