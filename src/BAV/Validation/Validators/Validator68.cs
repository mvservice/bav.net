﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator68.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator68 : ValidatorMulti
    {
        private readonly Validator00 m_exceptionValidator;

        public Validator68()
        {
            _validators.Add(new Validator00());
            _validators.Add(new Validator00(10, new System.Collections.ArrayList(new Object[] {2, 1, 2, 1, 2, 0, 0, 1})));
            m_exceptionValidator = new Validator00((int) 10, (int) -2, (int) 3, new[] {new System.Collections.ArrayList(new Object[] {2, 1})});
        }

        public override bool IsValid(Account account)
        {
            string accountID = LeftTrim(account);
            if (accountID != account.Number || accountID.Length < 6)
                return false;
            if (accountID.Length == 10 && accountID[3] == '9')
                return m_exceptionValidator.IsValid(account);
            if (accountID.Length == 9 && Int64.Parse(accountID) >= 400000000 && Int64.Parse(accountID) <= 499999999)
                return true;
            return base.IsValid(account);
        }

        protected override bool ContinueCondition(Account account)
        {
            switch (_current)
            {
                case 1:
                    string check = account.Number.Substring(account.Number.Length - 1);
                    if (Int32.Parse(check) >= 7)
                    {
                        _current++;
                        if (_current < _validators.Count)
                            return true;
                        return false;
                    }
                    break;
            }
            return true;
        }
    }
}