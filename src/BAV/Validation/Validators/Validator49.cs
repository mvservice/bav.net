﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator49.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator49 : Validator
    {
        private readonly Validator00 m_validator_00;
        private readonly Validator01 m_validator_01;

        public Validator49()
        {
            m_validator_00 = new Validator00();
            m_validator_01 = new Validator01();
        }

        public override bool IsValid(Account account)
        {
            return (Boolean) m_validator_00.IsValid(account) || (Boolean) m_validator_01.IsValid(account);
        }
    }
}