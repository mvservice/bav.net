﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator06.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator06 : ValidatorWeightedDefault
    {
        public Validator06()
            : this(11)
        {
        }

        public Validator06(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(3);
            _weight.Add(4);
            _weight.Add(5);
            _weight.Add(6);
            _weight.Add(7);
        }

        public Validator06(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator06(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 0)
        {
        }

        public Validator06(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator06(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, start, end, checkNumberPos, weight)
        {
        }

        protected override bool IsValid(string accountId)
        {
            Int32 check = m_aux%Modulo;
            if (check != 0)
            {
                check = (Modulo - check)%10;
            }
            Int32 proof = getCheckNumber(accountId);
            if (accountId == "0030925054")
                throw new ApplicationException(accountId + ":" + m_aux + "-" + Modulo + "-" + (m_aux%Modulo));

            return check == proof;
        }
    }
}