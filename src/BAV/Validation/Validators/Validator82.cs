﻿namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator82 : Validator
    {
        private Validator10 m_validator_10;
        private Validator33 m_validator_33;
    
        public Validator82()
        {
            m_validator_10 = new Validator10();
            m_validator_33 = new Validator33();
        }
    
        public override bool IsValid(Account account)
        {
            string accountID = this.PreNulls(account);
            if (accountID == "")
                return false;

            if (accountID.Substring(2, 2) == "99")
                return this.m_validator_10.IsValid(account);
            else
                return this.m_validator_33.IsValid(account);
        }
    }
}
