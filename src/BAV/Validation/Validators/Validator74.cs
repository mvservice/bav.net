﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator74.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator74 : Validator00
    {
        public Validator74()
            : this(10)
        {
        }

        public Validator74(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(1);
        }

        public Validator74(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : base(modulo, weight)
        {
        }

        public override bool IsValid(Account account)
        {
            return LeftTrim(account.Number).Length >= 6 && (Boolean) base.IsValid(account);
        }

        protected override bool IsValid(string accountId)
        {
            if (base.IsValid(accountId))
                return true;
            if (LeftTrim(accountId) != "6")
                return false;

            Int32 nextDecade = m_aux/10 + 1;
            Int32 nextHalfDecade = nextDecade*10 - 5;
            Int32 check = nextHalfDecade - m_aux;
            return check == getCheckNumber(accountId);
        }
    }
}