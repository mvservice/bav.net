﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator05 : Validator01
    {
        public Validator05()
            : this(10)
        {
        }
        public Validator05(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            this._weight.Add(7);
            this._weight.Add(3);
            this._weight.Add(1);
        }
         public Validator05(Int32 modulo,System.Collections.ArrayList weight)
            : base(modulo, weight)
        {
        }
    }
}
