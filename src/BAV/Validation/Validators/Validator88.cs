﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator88.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator88 : Validator
    {
        private readonly Validator06 m_validator_06a;
        private readonly Validator06 m_validator_06b;

        public Validator88()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            System.Collections.ArrayList weight2 = new System.Collections.ArrayList();
            weight1.Add(2);
            weight1.Add(3);
            weight1.Add(4);
            weight1.Add(5);
            weight1.Add(6);
            weight1.Add(7);
            weight2.Add(2);
            weight2.Add(3);
            weight2.Add(4);
            weight2.Add(5);
            weight2.Add(6);
            weight2.Add(7);
            weight2.Add(8);
            m_validator_06a = new Validator06(11, weight1, -2, 3);
            m_validator_06b = new Validator06(11, weight2, -2, 2);
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;
            if (accountID.Substring(2, 1) == "9")
                return m_validator_06b.IsValid(account);
            return m_validator_06a.IsValid(account);
        }
    }
}