﻿using System;

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator51Exception : ValidatorMulti
    {
        public Validator51Exception()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            System.Collections.ArrayList weight2 = new System.Collections.ArrayList();
            weight1.Add(2);
            weight1.Add(3);
            weight1.Add(4);
            weight1.Add(5);
            weight1.Add(6);
            weight1.Add(7);
            weight1.Add(8);
            weight2.Add(2);
            weight2.Add(3);
            weight2.Add(4);
            weight2.Add(5);
            weight2.Add(6);
            weight2.Add(7);
            weight2.Add(8);
            weight2.Add(9);
            weight2.Add(10);
            this._validators.Add(new Validator51ExceptionAux(11, weight1, -2, 2));
            this._validators.Add(new Validator51ExceptionAux(11, weight2, -2, 0));
        }

        public Boolean isExceptional(string accountID)
        {
            return accountID.Length > 2 && accountID.Substring(2, 1) == "9";
        }
    }
}
