﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorA0.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorA0 : Validator06
    {
        public ValidatorA0()
            : this(11)
        {
        }

        public ValidatorA0(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList())
        {
            _weight.Add(2);
            _weight.Add(4);
            _weight.Add(8);
            _weight.Add(5);
            _weight.Add(10);
            _weight.Add(0);
            _weight.Add(0);
            _weight.Add(0);
            _weight.Add(0);
        }

        public ValidatorA0(Int32 modulo, System.Collections.ArrayList weight)
            : base(modulo, weight)
        {
        }

        protected override bool isValid_accountID(string accountID)
        {
            return Int32.Parse(accountID.TrimStart('0')) < 1000 || base.isValid_accountID(accountID);
        }
    }
}