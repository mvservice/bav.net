﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator58.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class Validator58 : Validator02
    {
        public Validator58()
            : this(11)
        {
        }

        public Validator58(Int32 modulo)
            : this(modulo, new System.Collections.ArrayList(new Object[] {2, 3, 4, 5, 6, 0, 0, 0, 0}))
        {
        }

        public Validator58(
            Int32 modulo,
            System.Collections.ArrayList weight
            )
            : this(modulo, weight, -2)
        {
        }

        public Validator58(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start)
            : this(modulo, weight, start, 4)
        {
        }

        public Validator58(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end)
            : this(modulo, weight, start, end, -1)
        {
        }

        public Validator58(
            Int32 modulo,
            System.Collections.ArrayList weight,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos)
            : base(modulo, weight, start, end, checkNumberPos)
        {
        }

        public override bool IsValid(Account account)
        {
            return account.Number.Length >= 6 && (Boolean) base.IsValid(account);
        }
    }
}