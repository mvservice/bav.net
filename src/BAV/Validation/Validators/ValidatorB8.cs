﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorB8.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

namespace MVService.BankAccountValidator.Validation.Validators
{
    internal class ValidatorB8 : ValidatorMulti
    {
        public ValidatorB8()
        {
            System.Collections.ArrayList weight1 = new System.Collections.ArrayList();
            weight1.Add(2);
            weight1.Add(3);
            weight1.Add(4);
            weight1.Add(5);
            weight1.Add(6);
            weight1.Add(7);
            weight1.Add(8);
            weight1.Add(9);
            weight1.Add(3);

            _validators.Add(new Validator20(11, weight1));
            _validators.Add(new Validator29(10));
        }
    }
}