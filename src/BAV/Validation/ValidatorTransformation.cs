﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorTransformation.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    ///     Diese Abstakte Klasse Bietet die Unterstützung für  Algorhitmen welche Transformations Tabellen nutzen.
    /// </summary>
    internal abstract class ValidatorTransformation : ValidatorIteration
    {
        protected System.Collections.Hashtable _matrix;
        protected System.Collections.ArrayList _rowIteration;

        protected ValidatorTransformation()
            : this(new System.Collections.Hashtable())
        {
        }

        protected ValidatorTransformation(
            System.Collections.Hashtable matrix
            )
            : this(matrix, new System.Collections.ArrayList())
        {
        }

        protected ValidatorTransformation(
            System.Collections.Hashtable matrix,
            System.Collections.ArrayList rowIteration
            )
            : this(matrix, rowIteration, 0)
        {
        }

        protected ValidatorTransformation(
            System.Collections.Hashtable matrix,
            System.Collections.ArrayList rowIteration,
            Int32 start
            )
            : this(matrix, rowIteration, start, -2)
        {
        }

        protected ValidatorTransformation(
            System.Collections.Hashtable matrix,
            System.Collections.ArrayList rowIteration,
            Int32 start,
            Int32 end
            )
            : this(matrix, rowIteration, start, end, -1)
        {
        }

        protected ValidatorTransformation(
            System.Collections.Hashtable matrix,
            System.Collections.ArrayList rowIteration,
            Int32 start,
            Int32 end,
            Int32 checkNumberPos
            )
            : base(start, end, checkNumberPos)
        {
            _matrix = matrix;
            _rowIteration = rowIteration;
        }

        protected override void IterationStep(Int32 number, Int32 i)
        {
            System.Collections.Hashtable row = (System.Collections.Hashtable) _matrix[_rowIteration[i%_matrix.Count]];
            if (row.ContainsKey(number))
            {
                ProcessNumber(((Int32) row[number]));
            }
            else
            {
                ProcessNumber(0);
            }
        }

        protected abstract void ProcessNumber(Int32 transformedNumber);
        protected abstract override Boolean IsValid(String accountId);
    }
}