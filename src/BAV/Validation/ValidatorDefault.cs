﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator_Default.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation
{
    [Serializable]
    internal class ValidatorDefault : Validator
    {
        internal override ValidatorResult GetValidatorResult(Account account)
        {
            return new ValidatorResult(Validator_Result.UnknownAlgorithm, account);
        }

        public override bool IsValid(Account account)
        {
            return false;
        }
    }
}