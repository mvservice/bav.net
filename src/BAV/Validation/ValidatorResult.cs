﻿using System;

namespace MVService.BankAccountValidator.Validation
{
    internal class ValidatorResult
    {
        private Validator_Result m_code;
        private Account m_account;

        public ValidatorResult(Validator_Result code, Account account)
        {
            m_code = code;
            m_account = account;
        }
        public Validator_Result Code
        {
            get { return this.m_code; }
        }
        internal Account Account
        {
            get { return m_account; }
        }
        public Boolean isValid
        {
            get { return m_code == Validator_Result.Valid; }
        }
        public Boolean isInValid
        {
            get { return m_code == Validator_Result.Invalid; }
        }
        public Boolean isUnknown_Bank
        {
            get { return m_code == Validator_Result.UnknownBank; }
        }
        public Boolean isUnknown_Algorithm
        {
            get { return m_code == Validator_Result.UnknownAlgorithm; }
        }
        public Boolean isUnknown
        {
            get { return this.isUnknown_Algorithm || this.isUnknown_Bank; }
        }

    }
}
