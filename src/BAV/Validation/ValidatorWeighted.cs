﻿using System;

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    /// Diese Abstrakte Klasse Bietet die Unterstützung für Gewichtungs Algorhytmen.
    /// </summary>
    /// <remarks>
    /// isValid_accountID() wiederhollt von m_start bis m_end (beide Inklusive) 
    /// und ruft für jede einezlne nummer ProcessNumber() mit der Aktuellen nummer und der Aktuellen _weight auf.
    /// Die Werte von _weight werde sich drehen sobald
    /// _weight.Count &gt; abs(m_end-m_start)+1
    /// </remarks>
    internal abstract class ValidatorWeighted : ValidatorIteration
    {
        private readonly Int32 _modulo;
        protected System.Collections.ArrayList _weight = new System.Collections.ArrayList();
        private readonly Byte[] _weights;
        protected ValidatorWeighted()
            : this(10)
        {
        }

        protected ValidatorWeighted(int modulo, params byte[] weigth)
            : this(modulo, 0, weigth)
        {
        }

        protected ValidatorWeighted(int modulo, int start, params byte[] weigth)
            : this(modulo, start, (int)-1, weigth)
        {
        }

        protected ValidatorWeighted(int modulo, int start, int end, params byte[] weigth)
            : this(modulo, start, end, (int)-1, weigth)
        {
        }

        protected ValidatorWeighted(int modulo, int start, int end, int checkNumberPos, params byte[] weigth)
            : base(start, end, checkNumberPos)
        {
            _modulo = modulo;
            _weights = weigth;
        }

        protected byte[] Weights
        {
            get { return _weights; }
        }

        protected int Modulo
        {
            get { return _modulo; }
        }

        protected override void IterationStep(int number, int i)
        {
            ProcessNumber(number, _weights[i % _weights.Length]);
        }

        protected abstract void ProcessNumber(int number, int weight);
    }
}
