﻿using System;

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    /// Diese Abstkarte Klasse Bietet die Unterstützung um mehrere Algoritmen zu Benutzen
    /// </summary>
    /// <remarks>Füge den Algoritmus der System.Collection.Arraylist this._validators hinzu</remarks>
    internal abstract class ValidatorMulti : Validator
    {
        protected System.Collections.ArrayList _validators = new System.Collections.ArrayList();
        protected Int32 _current = 0;

        public override bool IsValid(Account account)
        {
            if (_validators.Count == 0)
                return false;
            for (_current = 0; _current < _validators.Count && this.ContinueCondition(account); _current++)
            {
                if ((bool)(((Validator)_validators[_current]).IsValid(account)))
                {
                    return true;
                }
            }
            return false;
        }
        protected virtual Boolean ContinueCondition(Account account)
        {
            return true;
        }
    }
}
