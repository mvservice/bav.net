﻿using System;

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    /// Das Resultat der Validierung
    /// </summary>
    [Serializable] 
    public enum Validator_Result : int
    {
        /// <summary>
        /// Validierung Fehlgeschlagen, Number und Bankleitzahl passen nicht 
        /// zusammen.
        /// </summary>
        Invalid = 0,
        /// <summary>
        /// Validierung OK
        /// </summary>
        Valid = 1,
        /// <summary>
        /// Bank ist Unbekannt
        /// </summary>
        UnknownBank = 2,
        /// <summary>
        /// Der Algorithmus ist Unbekannt
        /// </summary>
        UnknownAlgorithm = 3
    }
}