﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    ///     Diese Abstrakte Klasse ist die Vaterklasse für alle Validatoren
    /// </summary>
    [Serializable]
    internal abstract class Validator
    {
        protected Account _account;

        internal virtual ValidatorResult GetValidatorResult(Account account)
        {
            _account = account;
            Boolean isValid = IsValidType() && IsValid(account);
            if (isValid)
            {
                return new ValidatorResult(Validator_Result.Valid, account);
            }
            return new ValidatorResult(Validator_Result.Invalid, account);
        }

        protected Boolean IsValidType()
        {
            long accId;
            return long.TryParse(_account.Number, out accId);
        }

        public abstract bool IsValid(Account account);

        protected virtual string LeftTrim(Account account)
        {
            return LeftTrim(account.Number);
        }

        protected virtual string LeftTrim(string accountId)
        {
            return accountId.TrimStart('0');
        }

        protected Int32 CrossSum(Int32 number)
        {
            Int32 sum = 0;
            string strNumber = number.ToString();
            for (int i = 0; i < strNumber.Length; i++)
            {
                sum += Int32.Parse(strNumber.Substring(i, 1));
            }
            return sum;
        }

        protected string PreNulls(Account account)
        {
            return PreNulls(account.Number);
        }

        protected string PreNulls(string value)
        {
            return PreNulls(value, 10);
        }

        protected string PreNulls(string value, int size)
        {
            string strValue = value;
            if (strValue.Length > size)
                return "";
            Int32 lenght = strValue.Length;
            for (int i = 0; i < (size - lenght); i++)
            {
                strValue = "0" + strValue;
            }
            return strValue;
        }
    }
}