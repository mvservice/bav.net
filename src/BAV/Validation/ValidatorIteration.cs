﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidatorIteration.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Validation
{
    /// <summary>
    ///     Diese Abstrakte Klasse Bietet die Unterstützung für Wiederholungs Algorithmen
    ///     isValid_accountID() Wiederhollt von m_start bis m_end (beide Inklusive) und ruft für jede einzelne nummer
    ///     ProcessNumber() mit der Aktuellen nummer auf.
    /// </summary>
    /// <remarks>
    /// </remarks>
    [Serializable]
    internal abstract class ValidatorIteration : Validator
    {
        protected Int32 m_aux;
        protected Int32 m_checkNumberPos;
        protected Int32 m_end;
        protected Int32 m_start;

        public ValidatorIteration()
            : this(0, -1, -1)
        {
        }

        public ValidatorIteration(Int32 start)
            : this(start, -1, -1)
        {
        }

        public ValidatorIteration(Int32 start, Int32 end)
            : this(start, end, -1)
        {
        }

        public ValidatorIteration(Int32 start, Int32 end, Int32 pos)
        {
            m_start = start;
            m_end = end;
            m_checkNumberPos = pos;
        }

        public override bool IsValid(Account account)
        {
            string accountID = PreNulls(account);
            if (accountID == "")
                return false;
            return isValid_accountID(accountID);
        }

        protected virtual Boolean isValid_accountID(string accountID)
        {
            Int32 start = normalizePosition(m_start, accountID);
            Int32 end = normalizePosition(m_end, accountID);
            m_aux = 0;
            Int32 currentNumber = start;
            Int32 stepping = ((end - start < 0) ? -1 : +1);
            Int32 length = Math.Abs(end - start);
            for (int i = 0; i <= length; i++)
            {
                IterationStep(Int32.Parse(accountID.Substring(currentNumber, 1)), i);
                currentNumber += stepping;
            }
            return IsValid(accountID);
        }

        protected Int32 normalizePosition(Int32 i, String accountID)
        {
            if (i >= 0)
                return i;
            return accountID.Length + i;
        }

        protected abstract void IterationStep(Int32 number, Int32 i);
        protected abstract Boolean IsValid(String accountId);

        protected virtual Int32 getCheckNumber(String accountID)
        {
            return Int32.Parse(accountID.Substring(normalizePosition(m_checkNumberPos, accountID), 1));
        }
    }
}