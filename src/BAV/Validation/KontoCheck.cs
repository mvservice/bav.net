﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Validator_KontoCheck.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports



#endregion

namespace MVService.BankAccountValidator.Validation
{
    internal enum KontoCheck
    {
        Valid,
        Invalid,
        Unkown
    }
}