﻿using System;

namespace MVService.BankAccountValidator.Validation
{
    [Serializable]
    internal abstract class ValidatorKontoCheck : Validator
    {
        protected System.Collections.ArrayList _Quersumme = new System.Collections.ArrayList();

        internal override ValidatorResult GetValidatorResult(Account account)
        {
            _account = account;

            System.Collections.Hashtable returnTable = new System.Collections.Hashtable();

            returnTable.Add(KontoCheck.Valid, Validator_Result.Valid);
            returnTable.Add(KontoCheck.Invalid, Validator_Result.Invalid);
            returnTable.Add(KontoCheck.Unkown, Validator_Result.UnknownAlgorithm);
            ValidatorResult result;
            Boolean isValid =
                (Boolean)
                    this.IsValid(new Account(PreNulls(account), new Bank.Bank(account.Bank.Blz, account.Bank.Verfahren)));
            if (!IsValidType() || (isValid.GetType().ToString() == "System.Boolean" && isValid == false))
            {
                result = new ValidatorResult(Validator_Result.Invalid, account);
            }
            else if (isValid.GetType().ToString() == "System.Boolean" && isValid)
            {
                result = new ValidatorResult(Validator_Result.Valid, account);
            }
            else
            {
                Validator_Result returnCode;
                if (returnTable.ContainsKey(isValid))
                {
                    returnCode = Validator_Result.UnknownAlgorithm;
                }
                else
                {
                    returnCode = ((Validator_Result) returnTable[isValid]);
                }
                result = new ValidatorResult(returnCode, account);
            }
            return result;
        }
    }
}