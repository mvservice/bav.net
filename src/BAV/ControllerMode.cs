﻿namespace MVService.BankAccountValidator
{
    /// <summary>
    /// 
    /// </summary>
    public enum ControllerMode
    {
        /// <summary>
        /// 
        /// </summary>
        Hashtable,
        /// <summary>
        /// 
        /// </summary>
        Textfile,
        /// <summary>
        /// 
        /// </summary>
        MySql,
        /// <summary>
        /// 
        /// </summary>
        MsSql,
        /// <summary>
        /// 
        /// </summary>
        Binarysearch,
        /// <summary>
        /// 
        /// </summary>
        Oracle,
        /// <summary>
        /// 
        /// </summary>
        Odbc
    }
}