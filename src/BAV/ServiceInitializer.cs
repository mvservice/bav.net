﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceInitializer.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   12.04.2015 17:17
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using Catel.IoC;

#endregion

namespace MVService.BankAccountValidator
{
    /// <summary>
    /// 
    /// </summary>
    internal class ServiceInitializer : IServiceLocatorInitializer
    {
        #region IServiceLocatorInitializer Members

        #region Implementation of IServiceLocatorInitializer

        /// <summary>
        ///     Initializes the specified service locator.
        /// </summary>
        /// <param name="serviceLocator">The service locator.</param>
        public void Initialize(IServiceLocator serviceLocator)
        {
        }

        #endregion

        #endregion
    }
}