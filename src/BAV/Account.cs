﻿using MVService.BankAccountValidator.Validation;

namespace MVService.BankAccountValidator
{
    internal class Account
    {
        private readonly Bank.Bank _bank;
        private readonly string _number;

        public Bank.Bank Bank
        {
            get { return _bank; }
        }
        public string Number
        {
            get { return _number; }
        }

        
        public ValidatorResult GetValidatorResult()
        {
            // ToDo;
            return Bank.GetValidator().GetValidatorResult(this);
        }

        public Account(string number, Bank.Bank bank)
        {
            _number = number;
            _bank = bank;
        }

    }
}
