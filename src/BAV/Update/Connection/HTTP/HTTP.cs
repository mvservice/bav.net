﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP
{
    internal class HTTP : BAV
    {
        private Item.HTTPitem_Request m_requestTemplate;

        public HTTP(string server)
            : this(server, 80)
        {
        }
        public HTTP(string server, Int32 port)
            : this(server, port, 60)
        {
        }
        public HTTP(string server, Int32 port, Int32 timeout)
        {
            this.m_requestTemplate = new MVService.BAV.Update.Connection.HTTP.Item.HTTPitem_Request(new Connection(server, port, timeout));
        }

        public Item.HTTPitem_Request getRequestTemplate()
        {
            return this.m_requestTemplate;
        }

        public Item.HTTPitem_Request getRequest()
        {
            return (Item.HTTPitem_Request)this.m_requestTemplate.getClone();
        }
    }
}
