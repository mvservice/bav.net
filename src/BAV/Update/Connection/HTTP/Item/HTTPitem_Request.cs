﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP.Item
{
    internal class HTTPitem_Request : HTTPitem
    {
        private Connection m_connection;
        private HTTPitem_Response m_response = null;


        public HTTPitem_Request(Connection connection)
        {
            this.m_connection = connection;
            this.m_header = new Header.HTTPheader_Request();
            this.m_header.Add("host", this.m_connection.Server);
            this.m_header.Add("accept-encoding", "gzip");

        }


        public override Connection getConnection()
        {
            return this.m_connection;
        }

        public void setPath(string path)
        {
            this.m_header.setPath(path);
        }
        public void setMethod(string method)
        {
            this.m_header.setMethod(method);
        }
        public new HTTPitem_Request getClone()
        {
            HTTPitem_Request clone = new HTTPitem_Request(this.m_connection.getClone());
            clone.Header = this.m_header.getClone();
            return clone;
        }
        public HTTPitem_Response getResponse()
        {
            if (this.m_response == null)
            {
                if (!this._send())
                    return null;
                this.m_response = new HTTPitem_Response(this);
            }
            return this.m_response;     
        }
        private Boolean _send()
        {
            if (!this.m_connection.OpenOnce())
                return false;
            this.m_connection.setBlockingIO(false);
            if (!this.m_connection.Write(this.m_header.getString()))
                return false;
            this.m_connection.setBlockingIO(true);
            return true;
        }
    }
}
