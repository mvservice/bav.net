﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP.Item
{
    internal abstract class HTTPitem : BAV
    {
        protected Header.HTTPheader_Request m_header;

        public abstract Connection getConnection();

        public Header.HTTPheader Header
        {
            get { return this.m_header; }
            set { this.m_header = (Header.HTTPheader_Request)value; }
        }

    }
}
