﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP.Item
{
    internal class HTTPitem_Response : HTTPitem
    {
        private HTTPitem_Request m_request;
        //private string m_body;


        public HTTPitem_Response(HTTPitem_Request request)
        {
            this.m_request = request;
        }

        public override Connection getConnection()
        {
            return this.m_request.getConnection();
        }

        public Header.HTTPheader_Response getHeader()
        {
            return null;
        }
        public string getBody()
        {
            return "";
        }
        private Int32 getChunkSize()
        {
            return 0;
        }
    }
}
