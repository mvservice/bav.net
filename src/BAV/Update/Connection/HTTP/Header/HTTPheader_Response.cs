﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP.Header
{
    internal class HTTPheader_Response : HTTPheader
    {
        private Int32 m_statusCode = 200;
        private string m_reasonPhrase = "empty";

        public static HTTPheader_Response getInstance(string response)
        {
            Int32 firstBreak = response.IndexOf("\r\n");
            if(firstBreak == 0)
                return null;
            HTTPheader_Response responseHeader = new HTTPheader_Response();
            if (HTTPheader_Response.parseResponseLine(response.Substring(0, firstBreak), responseHeader))
                return null;
            response = response.Substring(firstBreak + 2);
            /*
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("/(.+) *?: *?(.+) *?\r\n/U");
            System.Text.RegularExpressions.Match match = reg.Match(response);
            string keys = match.Groups[1].Value;
            string values = match.Groups[2].Value;
            */

             /*^(?<name>\\w+):(?<value>\\w+)
             * if (preg_match_all('/(.+) *?: *?(.+) *?\r\n/U', $response, $matches))
             * {
                $keys   = $matches[1];
                $values = $matches[2];
                for ($i = 0; $i < count($keys); $i++) 
             * {
                    $responseHeader->add($keys[$i], $values[$i]);
                
                }
                }
             *
             * */

            return responseHeader;

        }
        private static Boolean parseResponseLine(string line, HTTPheader_Response header)
        {
            Int32 sp1 = line.IndexOf(" ");
            if (sp1 == 0)
                return false;
            Int32 sp2 = line.IndexOf(" ", sp1 + 1);
            if (sp2 == 0)
                return false;
            string version = line.Substring(5, sp1 - 5);
            string code = line.Substring(sp1 + 1, sp2 - sp1 - 1);
            string reason = line.Substring(sp2 + 1).Trim();
            Int32 iCode;
            try
            {
                iCode = System.Int32.Parse(code);
            }
            catch (FormatException ex)
            {
                ex.ToString();
                return false;
            }
            /*
             * PRüfen ob die Version auch Numerisch ist
             * */
            header.setVersion(version);
            header.StatusCode = iCode;
            header.ReasonPhrase = reason;
            return true;
        }

        public Int32 StatusCode
        {
            get { return this.m_statusCode; }
            set { this.m_statusCode = value; }
        }
        public string ReasonPhrase
        {
            get { return this.m_reasonPhrase; }
            set { this.m_reasonPhrase = value; }
        }
        public override string getFirstLine()
        {
            return "HTTP/"+this.m_version+" "+this.StatusCode+" "+this.ReasonPhrase;
        }
        public new HTTPheader_Response getClone()
        {
            HTTPheader_Response clone = ((HTTPheader_Response)base.getClone());
            clone.ReasonPhrase = this.ReasonPhrase;
            clone.StatusCode = this.StatusCode;
            return clone;
        }

    }
}
