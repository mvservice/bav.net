﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP.Header
{
    internal abstract class HTTPheader : BAV
    {
        private System.Collections.Hashtable m_header = new System.Collections.Hashtable();

        protected string m_version = "1.1";

        public new HTTPheader getClone()
        {
            HTTPheader clone = (HTTPheader)base.getClone();
            clone.setVersion(this.getVersion());
            clone.m_header = this.m_header;
            return clone;
        }
        public void setVersion(string version)
        {
            this.m_version = version;
        }
        public string getVersion()
        {
            return this.m_version;
        }
        public string getString()
        {
            return "";
        }
        public void Add(string name, string value)
        {
            if (this.m_header.ContainsKey(name.ToLower()))
            {
                ((System.Collections.ArrayList)(this.m_header[name.ToLower()])).Add(value);
            }
            else
            {
                this.m_header.Add(name.ToLower(), new System.Collections.ArrayList());
                ((System.Collections.ArrayList)(this.m_header[name.ToLower()])).Add(value);
            }
        }
        public Boolean Add_Once(string name, string value)
        {
            if (this.Exists(name))
                return false;
            this.Add(name.ToLower(), value);
            return true;
        }
        public void Remove(string name)
        {
            this.m_header.Remove(name.ToLower());
        }
        public string GMT(Object timeOrString)
        {
            String strTime = "";
            if (timeOrString.GetType().ToString() == "System.DateTime")
            {
                strTime = ((DateTime)(timeOrString)).ToUniversalTime() + " GMT";
            }
            return timeOrString.GetType().ToString() == "System.DateTime" ? strTime : ((string)timeOrString);
        }
        public Boolean Exists(string name)
        {
            return this.m_header.ContainsKey(name.ToLower());
        }
        public string Get(string name)
        {
            return this.Get(name, 0);
        }
        public string Get(string name, Int32 index)
        {
            return ((String)(((System.Collections.ArrayList)(this.m_header[name.ToLower()]))[index]));
        }
        public String GetIfExists(string name)
        {
            return this.GetIfExists(name, 0);
        }
        public String GetIfExists(string name, Int32 index)
        {
            if (this.Exists(name))
                return "";
            return this.Get(name, index);
        }
        public Boolean inValue(string name, string value)
        {
            if(! this.Exists(name))
                return false;
            
     
            value = value.ToLower();
            for (int i = 0; i < this.m_header.Count; i++)
            {
                if (((System.Collections.ArrayList)(this.m_header[i])).Contains(value))
                {
                    return true;
                }
            }
            /*
        foreach ($this->header[strtolower($name)] as $head) {
            if (strpos(strtolower($head), $value) !== false) {
                return true;
                
            }
             * }
            */
        
            return false;
        }
        public abstract string getFirstLine();
    }
}
