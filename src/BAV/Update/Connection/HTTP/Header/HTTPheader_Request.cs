﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection.HTTP.Header
{
    internal class HTTPheader_Request : HTTPheader
    {
        private string m_path = "/";
        private string m_method = "GET";

        public void setPath(string path)
        {
           // if (path.Substring(0, 3) == "../")
            {
               
                /* 
                $dir = explode('/', dirname($this->path));
                array_pop($dir);
                $this->setPath('/'.implode('/', $dir).substr($path, 3));
                */
            }
           // else
            {
                this.m_path = path;
            } 
        }
        public void setMethod(string method)
        {
            this.m_method = method;
        }
        public string getMethode()
        {
            return this.m_method;
        }
        public override string getFirstLine() 
        {
            return this.m_method+" "+this.m_path+" HTTP/"+this.m_version;
        }
        public new HTTPheader_Request getClone()
        {
            HTTPheader_Request clone = ((HTTPheader_Request)base.getClone());
            clone.setMethod(this.m_method);
            clone.setPath(this.m_path);
            return clone;
        }
    }
}
