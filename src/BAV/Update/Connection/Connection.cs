﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Update.Connection
{
    internal class Connection : BAV
    {
        private string m_server;
        private Int32 m_port;
        private Int32 m_timeOut;
        private System.Net.Sockets.TcpClient m_socket = null;

        public Connection(string server, Int32 port)
            : this(server, port, 30)
        {
        }
        public Connection(string server, Int32 port, Int32 timeout)
        {
            this.m_port = port;
            this.m_server = server;
            this.m_timeOut = timeout;
        }
        public new Connection getClone()
        {
            return new Connection( m_server, this.m_port, this.m_timeOut);
        }
        public Boolean setBlockingIO(Boolean doBlockingIO)
        {
            return false;
        }
        public Boolean isOpen()
        {
            return this.m_socket != null;
        }
        public string Server
        {
            get { return this.m_server; }
        }
        public Int32 Port
        {
            get { return this.m_port; }
        }
        public Boolean OpenOnce()
        {
            return this.isOpen() ? true : this.Open();
        }
        public Boolean Open()
        {
            if (this.isOpen())
            {
                if (this.isEndOfConnection())
                {
                    this.Close();
                    return this.Open();
                }
                else
                {
                    throw new System.IO.IOException("connection is already opened.");
                }
            }
            System.Net.Sockets.TcpClient tcpClient = new System.Net.Sockets.TcpClient(this.m_server, this.m_port);
            tcpClient.Connect(m_server,m_port);
            tcpClient.SendTimeout = m_timeOut;
            tcpClient.ReceiveTimeout = m_timeOut;
            this.m_socket = tcpClient;
            return true;
        }
        public Boolean Close()
        {
            if (!this.isOpen())
            {
                throw new System.IO.IOException("connection is not opened.");
            }

            this.m_socket.Close();

            this.m_socket = null;
            return true;
        }
        public Boolean isEndOfConnection()
        {
            return this.m_socket.Available != 0;
        }
        public Boolean Write(string data)
        {
            return true;
        }
        public string Read(Int32 bytes)
        {
            return "";
        }
        public string ReadLn()
        {
            return "";
        }
        public string readTillEnd()
        {
            return "";
        }
        public string SendRequest(String data)
        {
            return "";
        }
    }
}
