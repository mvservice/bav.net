﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MVService.BankAccountValidator.Update
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable] 
    public class Update
    {
        private System.Collections.Hashtable m_updateStatus = new System.Collections.Hashtable();
        private string m_updateFile = null;
        private Bank.BankFactory.BankFactory m_factory;
        private Boolean m_beQuit = false;
        private Boolean m_forceUpdate = false;
        // private Boolean m_doTest = false;
        private Boolean m_isUpToDate = true;
        private Boolean m_testFailed = false;
        private string m_message = "";
        private System.UriBuilder m_UriBuild;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factory"></param>
        public Update(Bank.BankFactory.BankFactory factory)
        {
            this._loadUpdateStatus();
            this.m_factory = factory;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool update()
        {
            m_UriBuild = new UriBuilder("http:\\", Settings.Default.Update_Host, 80, Settings.Default.Update_Path);
            if (!this._doHTTP())
                return false;
            if (this.m_updateFile == null)
                return true;
            this.printIn("updating datasource...");
            Bank.BankSaver.BankSaver saver = this.m_factory.getBankSaver();
            Boolean returnv = saver.saveFile(this.m_updateFile) && this._saveUpdateStatus();
            if (!returnv)
                this.printIn("updating failed!");
            /*
                        if (this.m_doTest)
                        {
                            Test.Factory.Test_Factory test = this.m_factory.getTest(this.m_updateFile);
                            if (this.m_beQuit)
                                test.beQuit();
                            if (!test.runTests())
                            {
                                this.printIn("Tests failed, but datastructure was updated!");
                                this.m_testFailed = true;
                                returnv = false;
                            }
                        }
             * */
            System.IO.File.Delete(this.m_updateFile);
            return returnv;
        }
        /// <summary>
        /// 
        /// </summary>
        public void beQuit()
        {
            this.m_beQuit = true;
        }
        /*
        public void doTest()
        {
            this.m_doTest = true;
        }*/
        /// <summary>
        /// 
        /// </summary>
        public void forceUpdate()
        {
            this.m_forceUpdate = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Boolean isUpToDate()
        {
            return this.m_isUpToDate;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Boolean testFailed()
        {
            return this.m_testFailed;
        }

        private void printIn(string message)
        {
            if (!this.m_beQuit)
            {
                m_message += message + "\n";
            }
        }
        private Boolean _doHTTP()
        {
            System.Net.WebClient whttp = new System.Net.WebClient();

            if (!this._isStale())
                return true;


            string body = whttp.DownloadString(m_UriBuild.Uri);
            string bodyMD5 = getMD5Hash(body);
            if (!this.m_forceUpdate && this.m_updateStatus.ContainsKey("bodyMD5") && ((string)this.m_updateStatus["bodyMD5"]) == bodyMD5)
                return true;
            if (!this.m_updateStatus.ContainsKey("bodyMD5"))
                this.m_updateStatus.Add("bodyMD5", bodyMD5);
            else
                this.m_updateStatus["bodyMD5"] = bodyMD5;

            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(/*":Bankleitzahlendatei .+href *= *\"([^\"]+pc\\.txt)\":sU"*/".*href[ ]*=[ ]*\"([^\"]*.txt)\"");
            System.Text.RegularExpressions.Match match = reg.Match(body);
            if (!match.Success)
                return false;

            m_UriBuild.Path = this.ParsePath(m_UriBuild.Path, match.Groups[1].Value);
            if (!_isStale())
                return true;

            this.m_isUpToDate = false;

            string tmpFile = this._getTemporaryFile();
            if (tmpFile == "")
                return false;
  

            try
            {
                whttp.DownloadFile(m_UriBuild.Uri, tmpFile);
            }
            catch (System.IO.IOException ex)
            {
                ex.ToString();
                return false;
            }

            this.m_updateFile = tmpFile;
            return true;
        }
        private string ParsePath(string oldPath, string path)
        {
            string i_path = "";
            System.Collections.ArrayList oldDir = new System.Collections.ArrayList();
            System.Collections.ArrayList dir = new System.Collections.ArrayList();
            int i = 0;
            int iAnfang = 0;
            int iTrenn = -1;
            for (i = 0; i < oldPath.Length; i++)
            {
                iTrenn++;
                if (oldPath.Substring(i, 1) == "/" || i == oldPath.Length - 1)
                {
                    if (i == oldPath.Length - 1)
                        iTrenn++;
                    oldDir.Add(oldPath.Substring(iAnfang, iTrenn));
                    iTrenn = 0;
                    iAnfang = ++i;
                }

            }
            oldDir.RemoveAt(oldDir.Count - 1);

            i = 0;
            iAnfang = 0;
            iTrenn = -1;
            for (i = 0; i < path.Length; i++)
            {
                iTrenn++;
                if (path.Substring(i, 1) == "/" || i == path.Length - 1)
                {
                    if (i == path.Length - 1)
                        iTrenn++;
                    dir.Add(path.Substring(iAnfang, iTrenn));
                    iTrenn = 0;
                    iAnfang = ++i;
                }

            }
            for (i = 0; i < oldDir.Count; i++)
            {
                if ((string)oldDir[i] == "")
                    oldDir.RemoveAt(i);
            }
            for (i = 0; i < dir.Count - 1; i++)
            {
                if ((string)dir[i] == "..")
                {
                    if (oldDir.Count > 0)
                        oldDir.RemoveAt(oldDir.Count - 1);
                    dir.RemoveAt(i);
                }
                if ((string)dir[i] == ".")
                {
                    dir.RemoveAt(i);
                }
            }

            for (i = 0; i < dir.Count; i++)
            {
                oldDir.Add(dir[i]);
            }
            i_path = "";
            for (i = 0; i < oldDir.Count; i++)
            {
                i_path += "/" + ((string)oldDir[i]);
            }
            i_path.ToString();
            return i_path;
        }
        private string getMD5Hash(string input)
        {
            MD5 md5Hasher = MD5.Create();

            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        private Boolean _isStale()
        {
            if (this.m_forceUpdate)
                return true;

            System.Net.HttpWebRequest HttpwReq = ((System.Net.HttpWebRequest)System.Net.WebRequest.Create(m_UriBuild.Uri));
            HttpwReq.Method = "HEAD";
            System.Net.HttpWebResponse HttpwRes = (System.Net.HttpWebResponse)HttpwReq.GetResponse();

            if (this.m_updateStatus.ContainsKey("lastModified"))
            {

                System.DateTime lastModfied_d = HttpwRes.LastModified;
                System.DateTime lastModfied_us = DateTime.Now;
                try
                {
                    lastModfied_us = ((DateTime)m_updateStatus["lastModified"]);
                }
                catch (FormatException ex)
                {
                    ex.ToString();
                    return false;
                }
                if (lastModfied_us >= lastModfied_d)
                    return false;

            }
            return true;
        }
        /*
        private Boolean _isStale(Connection.HTTP.HTTP http)
        {
            if (this.m_forceUpdate)
                return true;

            Connection.HTTP.Item.HTTPitem_Request headRequest = http.getRequest();
            headRequest.setMethod("HEAD");
            Connection.HTTP.Item.HTTPitem_Response headResponse = headRequest.getResponse();
            if (headResponse == null)
                return true;
            Connection.HTTP.Header.HTTPheader_Response headHeader = headResponse.getHeader();
            if (this.m_updateStatus.ContainsKey("lastModified") && headHeader.Exists("last-modified"))
            {
                string lastModified = headHeader.GMT(headHeader.Get("last-modified"));
                System.DateTime lastModfied_d = DateTime.Now;
                System.DateTime lastModfied_us = DateTime.Now;
                try
                {
                    lastModfied_d = DateTime.Parse(lastModified);
                    lastModfied_us = DateTime.Parse(((string)m_updateStatus["lastModified"]));
                }
                catch(FormatException ex)
                {
                    return false;
                }
                if(lastModfied_us >= lastModfied_d)
                    return false;

            }
            return true;
        }
         * */
        private Boolean _saveUpdateStatus()
        {
            this.m_updateStatus["lastModified"] = System.DateTime.Now;

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter myBinarySerializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            try
            {
                myBinarySerializer.Serialize(new System.IO.FileStream(Settings.Default.Update_Data, System.IO.FileMode.Create), this.m_updateStatus);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            return true;
        }
        private void _loadUpdateStatus()
        {
            System.IO.FileInfo fiInfo = new System.IO.FileInfo(Settings.Default.Update_Data);
            if (fiInfo.Exists == false)
                return;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter myBinarySerializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            this.m_updateStatus = ((System.Collections.Hashtable)myBinarySerializer.Deserialize(fiInfo.OpenRead()));
        }
        private string _getTemporaryFile()
        {
            String tmpFile = System.IO.Directory.GetCurrentDirectory() + "\\BAVUpdate";

            return tmpFile;
        }
        /// <summary>
        /// Gibt Zurück wann die Bankdaten das letzte mal Aktualisiert wurde.
        /// </summary>
        public DateTime LastUpdate
        {
            get
            {
                if (this.m_updateStatus.ContainsKey("lastModified"))
                    return ((DateTime)this.m_updateStatus["lastModified"]);
                else
                    return DateTime.MinValue;
            }
        }
    }
}
