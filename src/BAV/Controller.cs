﻿using System;
using MVService.BankAccountValidator.DataBase;
using MVService.BankAccountValidator.DataBase.Connection;
using MVService.BankAccountValidator.Validation;

namespace MVService.BankAccountValidator
{
    /// <summary>
    /// 
    /// </summary>
    public class Controller
    {
        private Bank.BankFactory.BankFactory m_factory;
        private static System.Data.Common.DbConnection m_connection = null;
        /// <summary>
        /// 
        /// </summary>
        public Controller()
        {
            this.setFactory(Settings.Default.Work_Modus);
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conMode"></param>
        public Controller(ControllerMode conMode)
        {
            this.setFactory(conMode);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankID"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public Boolean isInvalid(string bankID, string accountID)
        {
            ValidatorResult result = this.getValidatorResult(bankID, accountID);
            return result.isInValid;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankdID"></param>
        /// <returns></returns>
        public Boolean bankExists(string bankdID)
        {
            Bank.BankFactory.BankFactory factory = this.getFactory();
            return factory.bankExists(bankdID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conMode"></param>
        public void setFactory(ControllerMode conMode)
        {
            /*
            switch (conMode)
            {
                case ControllerMode.Hashtable:
                case ControllerMode.Textfile:
                    throw new NotImplementedException("Diese Aufrufe sind noch nicht Fehlerfrei Implementiert");

            }*/
            if (m_factory != null)
            {
                if (m_factory.GetType() == typeof(Bank.BankFactory.BankFactorySql))
                {
                    ((Bank.BankFactory.BankFactorySql)m_factory).Close();
                }
            }
            switch (conMode)
            {
                case ControllerMode.Hashtable:
                    this.m_factory = new Bank.BankFactory.hashtable.BankFactoryHashtable(Settings.Default.Controller_Hashfile);
                    Bank.BankFactory.BankFactory.Instance = new Bank.BankFactory.hashtable.BankFactoryHashtable(Settings.Default.Controller_Hashfile);
                    break;
                case ControllerMode.Textfile:
                    this.m_factory = new Bank.BankFactory.hashtable.BankFactoryHashtableBundesbank(Settings.Default.Controller_Textfile);
                    Bank.BankFactory.BankFactory.Instance = new Bank.BankFactory.hashtable.BankFactoryHashtableBundesbank(Settings.Default.Controller_Textfile);
                    break;
                case ControllerMode.Binarysearch:
                    this.m_factory = new Bank.BankFactory.BankFactoryBinarySearch(Settings.Default.Controller_Textfile);
                    Bank.BankFactory.BankFactory.Instance = new Bank.BankFactory.BankFactoryBinarySearch(Settings.Default.Controller_Textfile);
                    break;
                case ControllerMode.MySql:
                case ControllerMode.MsSql:
                case ControllerMode.Oracle:
                case ControllerMode.Odbc:
                    SQL sql = null;
                    switch (conMode)
                    {
                        case ControllerMode.MySql:
                            sql = new SQL_Connection_MySQL(m_connection);
                            break;
                        case ControllerMode.MsSql:
                            sql = new SQL_Connection_MsSQL(m_connection);
                            break;
                        case ControllerMode.Oracle:
                            sql = new SQL_Connection_Oracle(m_connection);
                            break;
                        case ControllerMode.Odbc:
                            sql = new SQL_Connection_ODBC(m_connection);
                            break;
                    }
                    this.m_factory = new Bank.BankFactory.BankFactorySql(sql);
                    Bank.BankFactory.BankFactory.Instance = new Bank.BankFactory.BankFactorySql(sql);
                    
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="showDatabaseDatas"></param>
        public static void ShowProperties(Boolean showDatabaseDatas)
        {
            fr_Eigenschaften.NoDataBaseDatas = !showDatabaseDatas;
            fr_Eigenschaften form = new fr_Eigenschaften();
            form.ShowDialog();
        }
        /// <summary>
        /// 
        /// </summary>
        public static void ShowTest()
        {
            fr_testSystem form = new fr_testSystem();
            form.ShowDialog();
        }
        /// <summary>
        /// 
        /// </summary>
        public static System.Data.Common.DbConnection Connection
        {
            get { return m_connection; }
            set { m_connection = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string DataBase
        {
            get { return Settings.Default.SQL_DB; }
            set { Settings.Default.SQL_DB = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string Password
        {
            set { Settings.Default.SQL_Pass = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string User
        {
            get { return Settings.Default.SQL_User; }
            set { Settings.Default.SQL_User = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string Host
        {
            get { return Settings.Default.SQL_HOST; }
            set { Settings.Default.SQL_HOST = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public static Int32 Port
        {
            get { return Settings.Default.SQL_PORT; }
            set { Settings.Default.SQL_PORT = value; }
        }
        internal ValidatorResult getValidatorResult(string bankID, string accountID)
        {
            Bank.BankFactory.BankFactory factory = this.getFactory();
            Bank.Bank bank = factory.getBank(bankID);
            return bank.GetValidatorResult(accountID);
        }
        /// <summary>
        /// Gibt den Bankname der BLZ zurück
        /// </summary>
        /// <param name="BLZ">Bankleitzahl der zu suchenden Datei</param>
        /// <returns>Bankname</returns>
        public string getBankname(string BLZ)
        {
            return this.getFactory().getBank(BLZ).Bankname;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankID"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public Validator_Result getValidationCode(string bankID, string accountID)
        {
            ValidatorResult result = this.getValidatorResult(bankID, accountID);
            return result.Code;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Bank.BankFactory.BankFactory getFactory()
        {
            if (this.m_factory == null)
                this.m_factory = Bank.BankFactory.BankFactory.Instance;
            return this.m_factory;
        }
    }
}
