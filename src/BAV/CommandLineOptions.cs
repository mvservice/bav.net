﻿using System;

namespace MVService.BankAccountValidator
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandLineOptions
    {
        private System.Collections.Hashtable m_paramters = new System.Collections.Hashtable();
        private System.Collections.Hashtable m_flags = new System.Collections.Hashtable();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="argumentArray"></param>
        public CommandLineOptions(String[] argumentArray)
            : this(argumentArray, new System.Collections.Hashtable())
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="argumentArray"></param>
        /// <param name="defaultParamters"></param>
        public CommandLineOptions(String[] argumentArray,System.Collections.Hashtable defaultParamters)
        {
            this.m_paramters = defaultParamters;
            for (int i = 0; i < argumentArray.Length; i++)
            {
                String args = argumentArray[i];
                String value = null;
                if ((i + 1 < argumentArray.Length) && argumentArray[i].Substring(0,1) != "-") 
                {
                    i++;
                    value = argumentArray[i];
                }
                int j = 1;
                for (j = 1; j < args.Length; j++)
                {
                    this.m_flags.Add(args.Substring(j, 1), true);
                }
                if (value != null && j == args.Length)
                {
                    this.m_paramters.Add(args.Substring(j - 1, 1), value);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        public Boolean getFlag(string flag)
        {
            return this.m_flags.ContainsKey(flag);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public string getParameter(string parameter)
        {
            return this.m_paramters.ContainsKey(parameter) ? ((string)this.m_paramters[parameter]) : "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        public Boolean exists(string paramter)
        {
            return this.m_paramters.ContainsKey(paramter);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramter"></param>
        /// <returns></returns>
        public string getParameterAsFile(string paramter)
        {
            string file = this.getParameter(paramter);
            return (file.Substring(0, 1) == "/" ? "" : System.IO.Directory.GetCurrentDirectory() + "/") + file;        
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 FlagCount
        {
            get { return this.m_flags.Count; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 ParametersCount
        {
            get { return this.m_paramters.Count; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 OptionCount
        {
            get { return this.FlagCount + this.ParametersCount; }
        }
    }
}
