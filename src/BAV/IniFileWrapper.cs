﻿using System;

namespace MVService.BankAccountValidator
{
    /// <summary>
    /// 
    /// </summary>
    public class IniFileWrapper
    {
        private readonly string _filename;
        private System.Collections.ArrayList m_content = new System.Collections.ArrayList();
        private System.Collections.Hashtable m_Daten = new System.Collections.Hashtable();
        /// <summary>
        /// 
        /// </summary>
        public string Filename
        {
            get { return this._filename; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public IniFileWrapper(string filename)
        {
            this._filename = filename;
            System.IO.FileStream fp = new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
            System.IO.StreamReader sreader = new System.IO.StreamReader(fp);
            while (!sreader.EndOfStream)
            {
                this.m_content.Add(sreader.ReadLine());
            }
            sreader.Close();
            fp.Close();
            this.ParseIni();
        }

        private void ParseIni()
        {
            string currentSection = "";
            
            Boolean breaker= false;
            foreach (Object line in m_content)
            {
                breaker = false;
                string zeile = (string)line;
                zeile = zeile.Trim();
                if (zeile == "" || zeile[0] == ';')
                    continue;

                if (zeile[0] == '[')
                {
                    currentSection = "";
                    for (int i = 0; i < zeile.Length; i++)
                    {
                        if (zeile[i] == ']')
                        {
                            currentSection = zeile.Substring(1, i - 1);
                            m_Daten.Add(currentSection, new System.Collections.Hashtable());
                            breaker = true;
                            break;
                        }
                    }
                }
                if (breaker == true)
                    continue;
                string currentKey = "";
                string values = "";
                Int32 lastFound = 0;
                Int32 length = 0;
                for (int i = 0; i < zeile.Length; i++)
                {
                    if (zeile[i] == '=')
                    {
                        currentKey = zeile.Substring(0, length ).Trim();
                        System.Collections.Hashtable reg = (System.Collections.Hashtable)this.m_Daten[currentSection];
                        reg.Add(currentKey, new System.Collections.ArrayList());
                        lastFound = i + 1;
                        values = zeile.Substring(i + 1).Trim();
                        length = 0;
                    }
                    if (zeile[i] == ',')
                    {
                        System.Collections.Hashtable reg = (System.Collections.Hashtable)this.m_Daten[currentSection];
                        System.Collections.ArrayList reg2 = (System.Collections.ArrayList)reg[currentKey];
                        reg2.Add(zeile.Substring(lastFound, length-1).Trim());
                        lastFound = i + 1;
                        length = 0;
                    }
                    length++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public System.Collections.ArrayList GetIniValue(string section, string key)
        {
            section.ToCharArray();
            return ((System.Collections.ArrayList)((System.Collections.Hashtable)this.m_Daten[section])[key]);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public System.Collections.Hashtable GetIniSection(string section)
        {

            return ((System.Collections.Hashtable)this.m_Daten[section]);
        }
     }
}
