﻿using System;

namespace MVService.BankAccountValidator.DataBase
{
    internal abstract class SQL
    {
        protected System.Data.Common.DbCommand m_command;
        protected System.Data.Common.DbDataReader m_reader;
        protected System.Data.Common.DbException m_exception;
         ~SQL()
        {
            this.Close();
        }
        public Boolean Query(string query)
        {
            return this.Query(query, true);
        }
        public virtual Boolean Query(string query, Boolean triggerError)
        {
            this.m_command.CommandText = query;
            try
            {
                if (this.m_reader != null && m_reader.IsClosed == false)
                    m_reader.Close();
                this.m_reader = this.m_command.ExecuteReader();
            }
            catch (System.Data.Common.DbException ex)
            {
                this.m_exception = ex;
                if (triggerError)

                    throw ex;
                else
                    return false;
            }
            return true;
        }
        public abstract Boolean Close();
        public virtual Int32 getFoundRows()
        {
            if (this.m_reader == null)
                throw new System.NullReferenceException("Bitte Erst einen Command absetzen.");
            try
            {
                return this.m_reader.RecordsAffected;
            }
            catch (System.Data.Common.DbException ex)
            {
                m_exception = ex;
            }
            return 0;
        }
        public virtual string Escape(string value)
        {
            return value;
        }
        public virtual Object[] FetchRow()
        {
            if (this.m_reader == null)
                throw new System.NullReferenceException("Bitte Erst einen Command absetzen.");
            Object[] objekts = new Object[m_reader.FieldCount];
            if (!this.m_reader.Read())
                return null;
            try
            {
                m_reader.GetValues(objekts);
            }
            catch (System.Data.Common.DbException ex)
            {
                m_exception = ex;
                return null;
            }
            return objekts;
        }
        public virtual Object[,] FetchRows()
        {
            Object[,] objekts = new Object[this.getFoundRows(),20];
            Int32 i = 0;
            Object[] row;
            while ((row = this.FetchRow()) != null)
            {
                for (int j = 0; j < row.Length; j++)
                {
                    objekts[i, j] = row[j];
                }
                    
                i++;
            }
            this.CloseQuery();
            return objekts;

        }
        public virtual Boolean CloseQuery()
        {
            this.m_reader.Close();
            return true;
        }
        public abstract System.Data.Common.DbException getError();
        public virtual Boolean ReplaceTable()
        {
            this.Query("DROP TABLE IF EXISTS " + Settings.Default.SQL_Table, false);
            this.m_reader.Close();
            return this.Query("CREATE TABLE " + Settings.Default.SQL_Table + " (" +
                                Settings.Default.SQL_BankID + " bigint ," +
                                Settings.Default.SQL_BankName + " varchar(60)," +
                                Settings.Default.SQL_Type + "   varchar(2));");
        }
    }
}
