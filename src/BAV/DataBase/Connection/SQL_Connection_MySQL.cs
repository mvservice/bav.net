﻿namespace MVService.BankAccountValidator.DataBase.Connection
{
    internal class SQL_Connection_MySQL : SQL_Connection
    {
        public SQL_Connection_MySQL()
            : this(null)
        {
        }
        public SQL_Connection_MySQL(System.Data.Common.DbConnection connection) 
            : base(connection)
        {
            this.m_command = new MySql.Data.MySqlClient.MySqlCommand();
            this.m_command.Connection = this.m_connection;
        }
        public new MySql.Data.MySqlClient.MySqlConnection getConnection()
        {
            return (MySql.Data.MySqlClient.MySqlConnection)base.getConnection();
        }
        protected override bool Connect()
        {
            this.m_connection = new MySql.Data.MySqlClient.MySqlConnection();
            System.Data.Common.DbConnectionStringBuilder Builder =  new System.Data.Common.DbConnectionStringBuilder(false);
            Builder.Add("Server", Settings.Default.SQL_HOST);
            Builder.Add("Port", Settings.Default.SQL_PORT);
            Builder.Add("Database", Settings.Default.SQL_DB);
            Builder.Add("Uid", Settings.Default.SQL_User);
            Builder.Add("Pwd", Settings.Default.SQL_Pass);
            try
            {
                this.m_connection.ConnectionString = Builder.ConnectionString;

                this.m_connection.Open();
            }
            catch (System.Data.Common.DbException ex)
            {
                this.m_exception = ex;
                throw ex;
            }
            return true;
        }


        public override bool ReplaceTable()
        {
            return base.ReplaceTable();
        }
    }

}
