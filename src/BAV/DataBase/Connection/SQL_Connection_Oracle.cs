﻿namespace MVService.BankAccountValidator.DataBase.Connection
{
    internal class SQL_Connection_Oracle : SQL_Connection
    {
        public SQL_Connection_Oracle()
            : this(null)
        {
        }
        public SQL_Connection_Oracle(System.Data.Common.DbConnection connection) 
            : base(connection)
        {
            this.m_command = new System.Data.OracleClient.OracleCommand();
            this.m_command.Connection = this.m_connection;
        }
        public new System.Data.OracleClient.OracleConnection getConnection()
        {
            return (System.Data.OracleClient.OracleConnection)base.getConnection();
        }
        protected override bool Connect()
        {
            this.m_connection = new System.Data.OracleClient.OracleConnection();
            System.Data.Common.DbConnectionStringBuilder Builder = new System.Data.Common.DbConnectionStringBuilder(false);
            Builder.Add("Server", Settings.Default.SQL_HOST);
            Builder.Add("Integrated Security", true);
            Builder.Add("User Blz", Settings.Default.SQL_User);
            Builder.Add("Password", Settings.Default.SQL_Pass);
            try
            {
                this.m_connection.ConnectionString = Builder.ConnectionString;

                this.m_connection.Open();
            }
            catch (System.Data.Common.DbException ex)
            {
                this.m_exception = ex;
                throw ex;
            }
            return true;
        }

        public override bool ReplaceTable()
        {
            return base.ReplaceTable();
        }
    }
}
