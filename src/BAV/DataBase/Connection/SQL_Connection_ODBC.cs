﻿namespace MVService.BankAccountValidator.DataBase.Connection
{
    internal class SQL_Connection_ODBC : SQL_Connection
    {
        public SQL_Connection_ODBC()
            : this(null)
        {
        }
        public SQL_Connection_ODBC(System.Data.Common.DbConnection connection) 
            : base(connection)
        {
            this.m_command = new System.Data.Odbc.OdbcCommand();
            this.m_command.Connection = this.m_connection;
        }
        public new System.Data.Odbc.OdbcConnection getConnection()
        {
            return (System.Data.Odbc.OdbcConnection)base.getConnection();
        }
        protected override bool Connect()
        {
            this.m_connection = new System.Data.Odbc.OdbcConnection();
            try
            {
                this.m_connection.ConnectionString = Settings.Default.SQL_ODBC_ConnectionString;
                this.m_connection.Open();
            }
            catch (System.Data.Common.DbException ex)
            {
                this.m_exception = ex;
                throw ex;
            }
            return true;
        }

        public override bool ReplaceTable()
        {
            return base.ReplaceTable();
        }
    }
}
