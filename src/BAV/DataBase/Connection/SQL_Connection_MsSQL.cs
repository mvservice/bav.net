﻿namespace MVService.BankAccountValidator.DataBase.Connection
{
    internal class SQL_Connection_MsSQL : SQL_Connection
    {
        public SQL_Connection_MsSQL()
            : this(null)
        {
        }
        public SQL_Connection_MsSQL(System.Data.Common.DbConnection connection) 
            : base(connection)
        {
            this.m_command = new System.Data.SqlClient.SqlCommand();
            this.m_command.Connection = this.m_connection;
        }
        public new System.Data.Common.DbConnection getConnection()
        {
            return (System.Data.SqlClient.SqlConnection)base.getConnection();
        }
        protected override bool Connect()
        {
            this.m_connection = new System.Data.SqlClient.SqlConnection();
            System.Data.Common.DbConnectionStringBuilder Builder = new System.Data.Common.DbConnectionStringBuilder(false);
            Builder.Add("Application Name", ".NET SQLClient BAV");
            Builder.Add("Server", Settings.Default.SQL_HOST);
            Builder.Add("database", Settings.Default.SQL_DB);
            Builder.Add("User Blz", Settings.Default.SQL_User);
            Builder.Add("Password", Settings.Default.SQL_Pass);
            try
            {
                this.m_connection.ConnectionString = Builder.ConnectionString;

                this.m_connection.Open();
            }
            catch (System.Data.Common.DbException ex)
            {
                this.m_exception = ex;
                throw ex;
            }
            return true;
        }

        public override bool ReplaceTable()
        {
            return base.ReplaceTable();
        }
    }
}
