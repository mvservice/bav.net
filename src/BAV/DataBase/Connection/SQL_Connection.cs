﻿using System;

namespace MVService.BankAccountValidator.DataBase.Connection
{
    internal abstract class SQL_Connection : SQL
    {
        protected System.Data.Common.DbConnection m_connection;
        

        public SQL_Connection(System.Data.Common.DbConnection connection)
        {
            if (connection == null)
                this.Connect();
            else
                this.m_connection = connection;
            
        }
        protected abstract Boolean Connect();
        public override Boolean Close()
        {
            this.m_connection.Close();
            return true;
        }
        public override System.Data.Common.DbException getError()
        {
            return m_exception;
        }
        public virtual System.Data.Common.DbConnection getConnection()
        {
            return this.m_connection;
        }
       
    }
}
