﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Bank.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;
using MVService.BankAccountValidator.Validation;

#endregion

namespace MVService.BankAccountValidator.Bank
{
    /// <summary>Die Bank mit allen Daten dazu</summary>
    [Serializable]
    public class Bank
    {
        /// <summary>
        ///     Der Name der Bank
        /// </summary>
        private readonly string _bankname;

        /// <summary>Die Blz der Bank (BLZ)</summary>
        private readonly string _blz;

        /// <summary>Das Verfahren</summary>
        private readonly string _verfahren;

        private Validator _validator;

        /// <summary>Construkter Erstellt eine neue Instanz</summary>
        /// <param name="blz">BLZ</param>
        /// <param name="verfahren">Der Algorhitmus</param>
        public Bank(string blz, string verfahren) : this(blz, verfahren, "")
        {
        }

        /// <summary>
        ///     Construkter Erstellt eine neue Instanz
        /// </summary>
        /// <param name="blz">BLZ</param>
        /// <param name="verfahren">Algorithmus</param>
        /// <param name="bankname">Bankname</param>
        public Bank(string blz, string verfahren, string bankname)
        {
            _blz = blz;
            _verfahren = verfahren;
            _bankname = bankname.Trim();
        }

        /// <summary>Das Verfahren nach dem Berechnet wird</summary>
        public string Verfahren
        {
            get { return _verfahren; }
        }

        /// <summary>Die BLZ</summary>
        public string Blz
        {
            get { return _blz; }
        }

        /// <summary>
        ///     Der Bankname
        /// </summary>
        public string Bankname
        {
            get { return _bankname; }
        }

        /// <summary>Hollt sich einen Account aus der Datenbank</summary>
        /// <param name="accountId">Die Number</param>
        internal Account GetAccount(string accountId)
        {
            return new Account(accountId, this);
        }

        internal virtual ValidatorResult GetValidatorResult(string accountId)
        {
            return GetAccount(accountId).GetValidatorResult();
        }

        internal Validator GetValidator()
        {
            if (_validator == null)
            {
                LoadValidator();
            }
            return _validator;
        }

        private void LoadValidator()
        {
            Type validatorType =
                Type.GetType(string.Format("MVService.BAV.Validator.Validators.Validator{0}", Verfahren), false, true);
            if (validatorType != null)
            {
                var validator = Activator.CreateInstance(validatorType) as Validator;
                if (validator != null)
                    _validator = validator;
            }

            if (_validator == null)
                _validator = new ValidatorDefault();
        }
    }
}