﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BankFactoryBinarySearch.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;
using System.Text;

#endregion

namespace MVService.BankAccountValidator.Bank.BankFactory
{
    internal class BankFactoryBinarySearch : BankFactory
    {
        private static readonly System.Collections.ArrayList m_array = new System.Collections.ArrayList();
        private readonly string m_file;
        private System.IO.FileStream m_fp;
        private Int32 m_lineLength;
        private Int32 m_lines;

        public BankFactoryBinarySearch(string bankfile)
        {
            m_file = bankfile;
        }

        private void assertOpenFile()
        {
            if (m_fp != null && m_fp.CanRead)
                return;

            System.IO.FileInfo fp = new System.IO.FileInfo(m_file);
            if (!fp.Exists)
                throw new System.IO.FileLoadException("Datei nicht gefunden");
            if (fp.Length < 20)
            {
                throw new System.IO.FileLoadException("Die Datei ist zu Klein");
            }
            m_fp = fp.Open(System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
            Byte[] dummyline = new Byte[1024];

            m_lineLength = m_fp.Read(dummyline, 0, 1024);
            for (int i = 0; i < dummyline.Length; i++)
            {
                if (dummyline[i] == 10 || dummyline[i] == 13)
                {
                    m_lineLength = i;
                    m_lineLength++;
                    break;
                }
            }
            m_lines = (int) Math.Floor(((double) fp.Length - 1)/m_lineLength);
        }

        public override Bank getBank(string id)
        {
            assertOpenFile();
            Bank bank = findBank(id, 0, m_lines);
            m_fp.Close();
            m_fp = null;
            return bank;
        }

        private Bank findBank(string id, int offset, int end)
        {
            if (end - offset < 0)
                return getDefaultBank(id);
            int line = offset + (end - offset)/2;
            m_fp.Seek(line*m_lineLength, System.IO.SeekOrigin.Begin);
            byte[] by_blz = new byte[8];
            m_fp.Read(by_blz, 0, Settings.Default.Bundesbank_BLZ_Length);
            string str_blz = Encoding.Default.GetString(by_blz);
            Bank bank;
            if (str_blz == "00000000")
            {
                bank = findBank(id, offset, line - 1);
                return bank.GetType().ToString() == "MVService.BAV.Bank.Bank_Default"
                    ? findBank(id, line + 1, end)
                    : bank;
            }
            if (id.Trim() == "")
                id = "0";
            if (str_blz.Trim() == "")
                str_blz = "0";
            if (Int32.Parse(str_blz) < Int32.Parse(id))
            {
                return findBank(id, line + 1, end);
            }
            if (Int32.Parse(str_blz) > Int32.Parse(id))
            {
                return findBank(id, offset, line - 1);
            }
            /*
                 * return BAV_BankFactory_Hashtable_Bundesbank::getBankByLine($blz.fread($this->fp, $this->lineLength - BAV_BUNDESBANK_BLZ_LENGTH), $this);
                 * */
            string puffer = "";
            byte[] by_data = new byte[m_lineLength - Settings.Default.Bundesbank_BLZ_Length];
            m_fp.Read(by_data, 0, m_lineLength - Settings.Default.Bundesbank_BLZ_Length);
            puffer = Encoding.Default.GetString(by_data);
            return BankFactoryHashtableBundesbank.getBankByLine(str_blz + puffer, this);
        }

        public override System.Collections.Hashtable getBankArray()
        {
            if (m_array.Count == 0)
            {
                m_array.Add(new BankFactoryHashtableBundesbank(m_file));
            }
            return ((BankFactoryHashtableBundesbank) (m_array[0])).getBankArray();
        }

        internal override BankSaver.BankSaver getBankSaver()
        {
            return new BankSaver.BankSaver_Bundesbank(m_file);
        }

        internal override Test.Factory.Test_Factory getTest()
        {
            return getTest(null);
        }

        internal override Test.Factory.Test_Factory getTest(string bankfile)
        {
            BankFactoryBinarySearch factory;
            if (bankfile == null)
            {
                factory = this;
            }
            else
            {
                factory = new BankFactoryBinarySearch(bankfile);
            }

            return new Test.Factory.Test_Factory_BinarySearch(factory);
        }

        public string getFile()
        {
            return m_file;
        }
    }
}