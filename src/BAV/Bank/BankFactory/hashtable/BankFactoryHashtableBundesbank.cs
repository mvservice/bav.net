﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BankFactoryHashtableBundesbank.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System.Text;

#endregion

namespace MVService.BankAccountValidator.Bank.BankFactory.hashtable
{
    internal class BankFactoryHashtableBundesbank : BankFactoryHashtable
    {
        public BankFactoryHashtableBundesbank(string file)
            : base(file)
        {
        }

        protected override void parse()
        {
            System.IO.FileInfo finfo = new System.IO.FileInfo(m_file);
            if (!finfo.Exists)
                return;

            System.IO.FileStream fp = finfo.Open(System.IO.FileMode.Open, System.IO.FileAccess.Read,
                System.IO.FileShare.ReadWrite);

            int length = Settings.Default.Bundesbank_Verfahren_Offset +
                         Settings.Default.Bundesbank_Verfahren_Length;
            byte[] byt = new byte[1024];
            int i = 0;

            int byteData;
            while ((byteData = fp.ReadByte()) != -1)
            {
                if (byteData == 10 || byteData == 13)
                {
                    string line = Encoding.Default.GetString(byt);
                    int b = 0;
                    for (b = 0; b < line.Length; b++)
                    {
                        if (line[b] == '\0')
                            break;
                    }
                    line = line.Substring(0, b);
                    byt = new byte[1024];
                    i = 0;
                    if (line.Length < length)
                        continue;
                    _add(getBankByLine(line, this));
                }
                else
                {
                    byt[i] = (byte) byteData;
                    i++;
                }
            }

            fp.Close();
        }

        public static Bank getBankByLine(string line, BankFactory factory)
        {
            string id = line.Substring(Settings.Default.Bundesbank_BLZ_Offset,
                Settings.Default.Bundesbank_BLZ_Length);
            string type = line.Substring(Settings.Default.Bundesbank_Verfahren_Offset,
                Settings.Default.Bundesbank_Verfahren_Length);
            string bankname = line.Substring(Settings.Default.Bundesbank_Bankname_Offset,
                Settings.Default.Bundesbank_Bankname_Length);
            return factory._newBankInstance(id, type, bankname);
        }

        internal override BankSaver.BankSaver getBankSaver()
        {
            return new BankSaver.BankSaver_Bundesbank(m_file);
        }
    }
}