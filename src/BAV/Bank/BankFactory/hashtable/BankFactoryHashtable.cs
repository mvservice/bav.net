﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BankFactory_Hashtable.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;

#endregion

namespace MVService.BankAccountValidator.Bank.BankFactory.hashtable
{
    internal class BankFactoryHashtable : BankFactory
    {
        private readonly System.Collections.Hashtable m_unsupported = new System.Collections.Hashtable();
        protected System.Collections.Hashtable m_banks = new System.Collections.Hashtable();
        protected string m_file = "";

        public BankFactoryHashtable(string file)
        {
            m_file = file;
        }

        private void assertParsedFile()
        {
            if (m_banks.Count != 0)
                return;
            System.IO.FileInfo fp = new System.IO.FileInfo(m_file);
            if (!fp.Exists)
                throw new System.IO.FileNotFoundException("Datei muss Existieren");
            parse();
        }

        public override Bank getBank(string id)
        {
            return bankExists(id) ? (Bank) m_banks[id] : getDefaultBank(id);
        }

        internal override Boolean bankExists(string id)
        {
            assertParsedFile();
            return m_banks.ContainsKey(id);
        }

        public override System.Collections.Hashtable getBankArray()
        {
            assertParsedFile();
            return m_banks;
        }

        protected virtual void parse()
        {
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter myBinarySerializer =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            System.IO.FileStream fstream = new System.IO.FileStream(m_file, System.IO.FileMode.Open,
                System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
            m_banks = ((System.Collections.Hashtable) myBinarySerializer.Deserialize(fstream));
            fstream.Close();
        }

        protected void _add(Bank bank)
        {
            if (bank.Blz == "00000000")
                return;
            if (m_unsupported.ContainsKey(bank.Blz))
                return;
            if (m_banks.ContainsKey(bank.Blz))
            {
                if (((Bank) (m_banks[bank.Blz])).Verfahren == bank.Verfahren)
                    return;
                m_unsupported.Add(bank.Blz, true);
                //throw new ApplicationException("Nicht Unterstütze BankID " + bank.Blz + " mit unterschiedlichem Algorhitmus " + bank.verfahren + ", " + ((Bank)(this.m_banks[bank.Blz])).verfahren);
                m_banks.Remove(bank.Blz);
                return;
            }
            m_banks.Add(bank.Blz, bank);
        }

        internal override BankSaver.BankSaver getBankSaver()
        {
            return new BankSaver.Converter.BankSaver_Converter_Hashtable(m_file);
        }
    }
}