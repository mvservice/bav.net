﻿using System;
using MVService.BankAccountValidator.DataBase;

namespace MVService.BankAccountValidator.Bank.BankFactory
{
    internal class BankFactorySql : BankFactory
    {
        protected SQL m_sql;

        public BankFactorySql(SQL sql)
        {
            this.m_sql = sql;
        }
        public Boolean Close()
        {
            return m_sql.Close();
            
        }
        internal override bool bankExists(string id)
        {
            Boolean result = true;
            result = result && this.m_sql.Query(
            "SELECT 1 " +
            "FROM "+Settings.Default.SQL_Table+ " " +
            "WHERE " + Settings.Default.SQL_BankID+ "='" + this.m_sql.Escape(id) + "';");
            result = result && this.m_sql.getFoundRows() >= 1;
            result = result && this.m_sql.CloseQuery() ;
            return result;
        }

        public override Bank getBank(string id)
        {
            if (!this.m_sql.Query(
            "SELECT " + Settings.Default.SQL_Type + "," + Settings.Default.SQL_BankName + " " +
            "FROM " + Settings.Default.SQL_Table + " " +
            "WHERE " + Settings.Default.SQL_BankID + "='" + this.m_sql.Escape(id) + "';")
            )
                return this.getDefaultBank(id);
            Object[] result;
            result = this.m_sql.FetchRow();
            m_sql.CloseQuery();
            if(result == null)
                return this.getDefaultBank(id);

            return this._newBankInstance(id, ((string)result[0]), ((string)result[1]));
        }
        internal override BankSaver.BankSaver getBankSaver()
        {
            return new BankSaver.Converter.BankSaver_Converter_SQL(this.m_sql);
        }
        public override System.Collections.Hashtable getBankArray()
        {
            if (!this.m_sql.Query(
                        "SELECT " +
                        Settings.Default.SQL_BankID +
                        "," + 
                        Settings.Default.SQL_Type + 
                        "," + 
                        Settings.Default.SQL_BankName + 
                        " " +
                        "FROM " + Settings.Default.SQL_Table + ";")
                        )
                return new System.Collections.Hashtable();
            System.Collections.Hashtable array = new System.Collections.Hashtable();
            Object[] result;
            int i=0;
            while ((result = this.m_sql.FetchRow()) != null)
            {
                array.Add(i, this._newBankInstance(((string)result[0]), ((string)result[1]), ((string)result[2])));
                i++;
            }
            m_sql.CloseQuery();
            return array;
        }
    }
}
