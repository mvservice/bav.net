﻿using System;
using MVService.BankAccountValidator.Validation;

namespace MVService.BankAccountValidator.Bank.BankFactory
{
    /// <summary>
    /// Die Klasse welche alle Banken Enthält und die Hauptarbeit durchführt
    /// </summary>
    public abstract class BankFactory
    {
        /// <summary>
        /// Gibt die Bank mit der Übergebenden BLZ wieder
        /// </summary>
        /// <param name="id">BLZ</param>
        /// <returns>Gibt die Bank zurück welche die Übergebende BLZ hat</returns>
        public abstract Bank getBank(string id);

        internal abstract BankSaver.BankSaver getBankSaver();
        /// <summary>
        /// Gibt ein Array der Banken wieder
        /// </summary>
        /// <returns>Alle in der Factory Geladenen Banken</returns>
        public abstract System.Collections.Hashtable getBankArray();

        internal virtual Boolean bankExists(string id)
        {
            Bank bank = this.getBank(id);
            ValidatorResult result = bank.GetValidatorResult("5034566");
            return !result.isUnknown_Bank;
        }
        /// <summary>
        /// Ruft bzw Setzt die Standard Instanz der BankFactory
        /// </summary>
        internal static BankFactory Instance
        {
            get
            {
                if (bav_static.BankFactory == null)
                {
                    throw new ApplicationException("No default factory was defined.");
                }
                return bav_static.BankFactory;
            }
            set { bav_static.BankFactory = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal protected Bank getDefaultBank(string id)
        {
            return new BankDefault(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns>Eine Bank</returns>
        public Bank _newBankInstance(string id, string type)
        {
            return new Bank(id, type);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">BLZ</param>
        /// <param name="type">TYPE</param>
        /// <param name="bankname">Bankname</param>
        /// <returns>Eine Bank</returns>
        public Bank _newBankInstance(string id, string type,string bankname)
        {
            return new Bank(id, type,bankname);
        }
        internal virtual Test.Factory.Test_Factory getTest()
        {
            return this.getTest(null);
        }
        internal virtual Test.Factory.Test_Factory getTest(string bankfile)
        {
            return new MVService.BAV.Test.Factory.Test_Factory(this, bankfile);
        }

        
    }
}
