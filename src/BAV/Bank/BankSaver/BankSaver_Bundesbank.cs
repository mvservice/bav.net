﻿using System;

namespace MVService.BankAccountValidator.Bank.BankSaver
{
    internal class BankSaver_Bundesbank : BankSaver
    {
        private string m_file;

        public BankSaver_Bundesbank(string file)
        {
            m_file = file;
        }

        public override bool saveFile(string bankfile)
        {
            Boolean ok = true;
            System.IO.FileInfo fiInf= new System.IO.FileInfo(bankfile);
            try
            {
                fiInf.CopyTo(this.m_file, true);
            }catch(Exception ex)
            {
                ex.ToString();
                ok = false;
            }
            return ok;
        }
    }
}
