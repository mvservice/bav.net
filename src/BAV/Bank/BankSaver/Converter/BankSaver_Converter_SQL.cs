﻿using System;
using MVService.BankAccountValidator.DataBase;

namespace MVService.BankAccountValidator.Bank.BankSaver.Converter
{
    internal class BankSaver_Converter_SQL : BankSaver_Converter
    {
        private SQL m_sql;
        private BankFactory.BankFactorySql m_bankfactory;
        public BankSaver_Converter_SQL(SQL sql)
        {
            m_sql = sql;
            this.m_bankfactory = new MVService.BAV.Bank.BankFactory.BankFactorySql(sql);
        }
        public SQL SQL
        {
            get { return m_sql; }
        }
        public override Boolean saveFactory(BankFactory.BankFactory factory)
        {
            return /*this.m_sql.ReplaceTable() && */base.saveFactory(factory);
        }
        protected override bool _saveBank(Bank bank)
        {
            String type = this.m_sql.Escape(bank.Verfahren);
            string bankID = bank.Blz;
            String bankName = bank.Bankname;
            if (!this.m_bankfactory.bankExists(bankID))
            {
                return this.m_sql.Query(
                        "INSERT INTO " + Settings.Default.SQL_Table + " " +
                          "(" + Settings.Default.SQL_BankID + "," + Settings.Default.SQL_BankName + "," + Settings.Default.SQL_Type + ")" +
                          "VALUES ('" + bankID + "', '" + bankName + "', '" + type + "')") && this.m_sql.CloseQuery();
            }
            else
            {
                return this.m_sql.Query(
                        "UPDATE " + Settings.Default.SQL_Table + " " +
                          "SET " + Settings.Default.SQL_BankName + " = '" + bankName + "' " +
                          "SET " + Settings.Default.SQL_Type + " = '" + type + "' " +
                          "WHERE " + Settings.Default.SQL_BankID + "='" + bankID + "' " +
                          "; ") && this.m_sql.CloseQuery();
            }
        }
    }
}
