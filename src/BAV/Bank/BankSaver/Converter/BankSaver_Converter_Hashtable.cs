﻿using System;

namespace MVService.BankAccountValidator.Bank.BankSaver.Converter
{
    internal class BankSaver_Converter_Hashtable : BankSaver_Converter
    {
         private string m_file;
              
        public BankSaver_Converter_Hashtable(string file)
        {
            this.m_file = file;
        }

        protected override Boolean _saveBank(Bank bank)
        {
            return false;
        }

        protected override Boolean _saveArray(System.Collections.Hashtable bankArray)
        {
            System.Collections.Hashtable hashtable = new System.Collections.Hashtable();
            foreach (System.Collections.DictionaryEntry bank in bankArray)
            {
                hashtable.Add(((Bank)bank.Value).Blz, new Bank(((Bank)bank.Value).Blz, ((Bank)bank.Value).Verfahren, ((Bank)bank.Value).Bankname));
            }
            /*
            for(int i= 0; i < bankArray.Count; i++)
            {
                hashtable.Add(((Bank)bankArray[i]).Blz, new Bank(((Bank)bankArray[i]).Blz, ((Bank)bankArray[i]).verfahren));
            }
             * */
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter myBinarySerializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.FileStream fstream = new System.IO.FileStream(this.m_file, System.IO.FileMode.Create,System.IO.FileAccess.ReadWrite,System.IO.FileShare.ReadWrite);
            try
            {
                myBinarySerializer.Serialize(fstream, hashtable);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            fstream.Close();
            return true;
        }
    }
}
