﻿using System;

namespace MVService.BankAccountValidator.Bank.BankSaver.Converter
{
    internal abstract class BankSaver_Converter : BankSaver
    {
        public override bool saveFile(string bankfile)
        {
            BankFactory.hashtable.BankFactoryHashtableBundesbank factory = new BankFactory.hashtable.BankFactoryHashtableBundesbank(bankfile);
            return this.saveFactory(factory);
        }
        public virtual Boolean saveFactory(BankFactory.BankFactory bankfactory)
        {
            System.Collections.Hashtable bankArray = bankfactory.getBankArray();
            if (bankArray.Count == 0)
                return false;
            return this._saveArray(bankArray);
        }
        protected virtual Boolean _saveArray(System.Collections.Hashtable bankArray)
        {
            Boolean ok = true;
            foreach (System.Collections.DictionaryEntry bank in bankArray)
            {
                ok = this._saveBank(((Bank)bank.Value)) && ok;
            }
            /*
            for (int i = 0; i < bankArray.Count; i++)
            {
                ok = this._saveBank(((Bank)bankArray[i])) && ok;
            }
             * */
            return ok;
        }
        protected abstract Boolean _saveBank(Bank bank);
    }
}
