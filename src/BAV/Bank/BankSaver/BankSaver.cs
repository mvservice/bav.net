﻿using System;

namespace MVService.BankAccountValidator.Bank.BankSaver
{
    internal abstract class BankSaver
    {
        public abstract Boolean saveFile(string bankfile);
    }
}
