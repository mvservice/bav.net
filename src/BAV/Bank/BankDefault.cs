﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Bank_Default.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;
using MVService.BankAccountValidator.Validation;

#endregion

namespace MVService.BankAccountValidator.Bank
{
    [Serializable]
    internal class BankDefault : Bank
    {
        public BankDefault(string blz)
            : base(blz, "")
        {
        }

        internal override ValidatorResult GetValidatorResult(string accountId)
        {
            return new ValidatorResult(Validator_Result.UnknownBank, GetAccount(accountId));
        }
    }
}