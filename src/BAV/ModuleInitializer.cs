﻿using Catel.IoC;
using MVService.BankAccountValidator;

/// <summary>
/// Used by the ModuleInit. All code inside the Initialize method is ran as soon as the assembly is loaded.
/// </summary>
public static class ModuleInitializer
{
    
    /// <summary>
    /// Initializes the module.
    /// </summary>
    public static void Initialize()
    {
        var serviceLocator = ServiceLocator.Default;
        var init = new ServiceInitializer();
        init.Initialize(serviceLocator);
    }
}