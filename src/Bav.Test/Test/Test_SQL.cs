﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Test
{
    internal class Test_SQL : Test
    {
        protected SQL.SQL m_sql;
        protected string m_sql_createTable = "CREATE TABLE BAV_test (a int, b int)";
        protected string m_sql_dropTable   = "DROP TABLE IF EXISTS BAV_test";
        protected string m_sql_insertRows  = "INSERT INTO BAV_test (a, b) VALUES (1, -1)";
        protected string m_sql_readRows    = "SELECT a, b FROM BAV_test";
        protected string m_sql_deleteRows  = "DELETE FROM BAV_test";

        Test_SQL(SQL.SQL sql) : base()
        {
            m_sql = sql;
        }
        public override bool runTests()
        {
            this.printTitle(this.m_sql.ToString());
            this.m_sql.Query(this.m_sql_dropTable, false);
            this.Query(this.m_sql_createTable);
            this.Read(this.m_sql_readRows, new Object[,] { { null } });
            this.Query(this.m_sql_insertRows);
            this.Read(this.m_sql_readRows, new Object[,] {  { 1,-1} });
            this.Query(this.m_sql_insertRows);
            this.Read(this.m_sql_readRows, new Object[,] {  { 1, -1 }, { 1, -1 } });
            this.Query(this.m_sql_deleteRows);
            this.Read(this.m_sql_readRows, new Object[,] { { null } });
            this.Query(this.m_sql_dropTable);
            return true;

        }
        private void Query(string sql)
        {
            if(!this.m_sql.Query(sql))
                this.printError(this.m_sql.getError().Message);

        }
        private void Read(string sql, Object[,] expectedResult)
        {
            this.Query(sql);
            Object[,] foundRows = this.m_sql.FetchRows();
            this.m_sql.CloseQuery();
            if (!Object.Equals(foundRows, expectedResult))
            {
                this.printError("unexpected Result");
                this.println(foundRows.ToString());
            }
        }

    }
}
