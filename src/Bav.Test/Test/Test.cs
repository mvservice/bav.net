﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Test
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message">Die Neue abgeschickte Nachricht</param>
    /// <param name="complete_Message">Alle bisher Gesammelten Nachrichten</param>
    internal delegate void TestMessageSendEventHandler(String message,string complete_Message);
    /// <summary>
    /// Stellt die Abstakte Vaterklasse für die Tests da.
    /// </summary>
    internal abstract class Test : BAV
    {
        /// <summary>
        /// Gibt die Zeit in Millisekunden an in der Das Script lief.
        /// </summary>
        protected Int64 m_time;
        private DateTime m_date;
        private Int16 m_spaces = 4;
        private Boolean m_beQuit;
        private string m_message = "";
        /// <summary>
        /// Initiator
        /// </summary>
        public Test()
        {
        }
        /// <summary>
        /// Beenden den Aufruf sofort;
        /// </summary>
        public void beQuiet()
        {
            m_beQuit = true;
        }
        /// <summary>
        /// Führt die Tests durch
        /// </summary>
        /// <returns>Zeigt an ob die Tests Erfolgreich waren.</returns>
        public abstract Boolean runTests();
        /// <summary>
        ///  Enthält die Nachrichten und ausgaben
        /// </summary>
        public string Message
        {
            get { return m_message; }
        }
        /// <summary>
        /// Zeichnet Eine leere Linie
        /// </summary>
        protected void println()
        {
            this.println("");
        }
        /// <summary>
        /// Zeichnet eine neue Zeile mit dem Übergebenden Text
        /// </summary>
        /// <param name="text">Zu Druckender Text</param>
        protected void println(string text)
        {
            string mess = "";
            if (m_beQuit == true)
                return;
            for (int i = 0; i < m_spaces; i++)
            {
                mess += " ";
            }
            mess += text + "\r\n";
            m_message += mess;
            if(TestMessageSend != null)
            TestMessageSend(mess,m_message);
        }
        protected void printElem(string text)
        {
            string mess = "";
            if (m_beQuit == true)
                return;
            for (int i = 0; i < m_spaces; i++)
            {
                mess += " ";
            }
            mess += text + "  ,";
            m_message += mess;
            if (TestMessageSend != null)
            TestMessageSend(mess,m_message);
        }
        internal void printTitle(string title)
        {
            this.m_spaces -= 4;
            this.println();
            this.println();
            string line = "**";
            for (int i = 0; i < title.Length; i++)
            {
                line += "*";
            }
            line += "**";
            string text = "* " + title + " *";
            this.println(line);
            this.println(text);
            this.println(line);
            this.m_spaces += 4;
        }
        internal void printBenchmark(string text)
        {
            Int64 s = this.m_time;
            Int64 ms = this.m_time - s * 1000;
            this.println(text + " [" + s + "s " + ms + "ms]");
        }
        internal void printError(string error)
        {
            this.m_spaces -= 3;
            this.println("-> " + error);
            this.m_spaces += 3;
        }
        internal void kill(string error)
        {
            this.printError(error);
            throw new Exception("Terminatet");
        }
        internal void TimeStart()
        {
            this.m_date = DateTime.Now;
        }
        internal void TimeStop()
        {
            Int32 stunden = (DateTime.Now.Hour - m_date.Hour);
            Int32 minuten = (DateTime.Now.Minute - m_date.Minute) + (stunden * 60);
            Int32 sekonds = (DateTime.Now.Second - m_date.Second) + (minuten * 60);
            m_time = (DateTime.Now.Millisecond - m_date.Millisecond) + (sekonds * 1000);
        }
        /// <summary>
        /// Wird ausgelösst sobald eine neue Nachricht geschrieben wurde.
        /// </summary>
        public event TestMessageSendEventHandler TestMessageSend;

    }
}
