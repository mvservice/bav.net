﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Test
{
    internal class checkValidators : Test
    {
        protected System.Collections.Hashtable m_knownBanks = new System.Collections.Hashtable();
        protected Bank.BankFactory.BankFactory m_bankFactory;
        private System.Collections.ArrayList m_allVerfahren = new System.Collections.ArrayList(new Object[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "C0", "C1", "C2" });
        protected System.Collections.Hashtable m_implementetBanks = new System.Collections.Hashtable();
        public checkValidators(TestMessageSendEventHandler handle)
        {
            this.TestMessageSend += handle;
            this.m_bankFactory = Bank.BankFactory.BankFactory.Instance;
            System.Collections.Hashtable bankArray = this.m_bankFactory.getBankArray();
            this.printTitle("Lade Banken");
            if (bankArray.Count != 0)
            {
                foreach (System.Collections.DictionaryEntry bank in bankArray)
                {
                    if (!this.m_knownBanks.ContainsKey(((Bank.Bank)(bank.Value)).Verfahren))
                        this.m_knownBanks.Add(((Bank.Bank)(bank.Value)).Verfahren, bank.Value);
                }
            }
            this.test_findParseErrors();

            this.test_findWrongImplementations();

        }
        /// <summary>
        /// NICHT BENUTZEN NICHT IMPLEMENTIERT
        /// </summary>
        /// <returns></returns>
        public override bool runTests()
        {
            throw new NotImplementedException("The method or operation is not implemented.");
        }
        private void test_findParseErrors()
        {
            this.printTitle("Starte Alle Validatoren um Fehler zu finden");
            System.Random rand = new Random();
            for (int i = 0; i < this.m_allVerfahren.Count; i++)
            {
                string verfahren = m_allVerfahren[i].ToString();
                this.printElem(verfahren);
                Bank.Bank bank = null;
                if (m_knownBanks.ContainsKey(verfahren))
                    bank = (Bank.Bank)this.m_knownBanks[verfahren];
                else
                    bank = new Bank.Bank("0", verfahren);
                this.m_implementetBanks.Add(verfahren, bank);

                for (int c = 0; c < 10; c++)
                {
                    Account account = bank.GetAccount(((double)(rand.Next(0, 999999) * rand.NextDouble())).ToString());
                    account.GetValidatorResult();
                }
            }
        }
        private void test_findWrongImplementations()
        {
            this.printTitle("Versuche mit ein paar TestAccount falsche Implementierungen zu finden.");
            IniFileWrapper verifyIni = new IniFileWrapper("data\\verify.ini");
            System.Collections.Hashtable validTests = verifyIni.GetIniSection("valid");
            System.Collections.Hashtable invalidTests = verifyIni.GetIniSection("invalid");

            this.println("Prüfe Gültige Kontonummern...");
            System.Collections.ArrayList notValidResult = this.__testNumbers(validTests, Validator.Validator_Result.Valid);
            this.println("Prüfe Ungültige Kontonummern...");
            System.Collections.ArrayList notInValidResult = this.__testNumbers(invalidTests, Validator.Validator_Result.Invalid);
            this.println("Nicht Gültige Kontonummern...");
            this.__printUnexpectedResults(notValidResult);
            this.println("Nicht Ungültige Kontonummern...");
            this.__printUnexpectedResults(notInValidResult);
        }
        private System.Collections.ArrayList __testNumbers(System.Collections.Hashtable numbers, Validator.Validator_Result result)
        {
            System.Collections.ArrayList unexpectedResults = new System.Collections.ArrayList();
            foreach (System.Collections.DictionaryEntry entry in numbers)
            {
                System.Collections.ArrayList accountIDs = (System.Collections.ArrayList)entry.Value;
                string typeOrBankID = (string)entry.Key;
                Bank.Bank bank = null;
                if (typeOrBankID.Length <= 2)
                {
                    typeOrBankID = (typeOrBankID.Length < 2 ? "0" : "") + typeOrBankID;
                    bank = (Bank.Bank)this.m_implementetBanks[typeOrBankID];
                }
                else
                    bank = this.m_bankFactory.getBank(typeOrBankID);

                println(bank.Verfahren);
                int k = 1;
                foreach (string accountID in accountIDs)
                {
                    printElem("" + k);
                    k++;
                    Account account = bank.GetAccount(accountID);
                    Validator.ValidatorResult check = account.GetValidatorResult();
                    if (check.Code == MVService.BAV.Validator.Validator_Result.UnknownAlgorithm)
                    {
                        unexpectedResults.Add(check);
                        break;
                    }
                    if (check.Code != result)
                        unexpectedResults.Add(check);
                }
                println();
            }



            return unexpectedResults;
        }
        private void __printUnexpectedResults(System.Collections.ArrayList results)
        {
            foreach (Validator.ValidatorResult result in results)
            {
                Account account = result.Account;
                Bank.Bank bank = account.Bank;
                this.println("      Code: " + result.Code);
                this.println("      verfahren: " + bank.Verfahren);
                this.println("   Bank Blz: " + bank.Blz);
                this.println("Account Blz: " + account.Number);
                this.println();

            }
        }
    }
}
