﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Test.Factory
{
    internal class Test_SQLFactory : Test
    {
        protected System.Collections.ArrayList m_sqlarray;
        public Test_SQLFactory(System.Collections.ArrayList sqlarray)
        {
            m_sqlarray = sqlarray;
        }

        public override bool runTests()
        {
            foreach (System.Collections.DictionaryEntry sql in m_sqlarray)
            {
                Test_Factory factoryTest = new Test_Factory(new Bank.BankFactory.BankFactorySql((SQL.SQL)sql.Value));
                factoryTest.printTitle(((SQL.SQL)sql.Value).GetType().ToString());
                factoryTest.runTests();
            }
            return true;
        }
    }
}
