﻿#region Header

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Test_Factory.cs" company="Visitmedia | Thomas Moracki">
//   Copyright (c) 2014 - 2015 Visitmedia. All rights reserved.
//   Solution:       BAV
//   Projekt:        BAV
//   Ersteller:      Marc Völker
//   Erstelldatum:   29.01.2013 15:08
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

#endregion

#region Namespace Imports

using System;
using System.Text;

#endregion

namespace MVService.BAV.Test.Factory
{
    internal class Test_Factory : Test
    {
        protected String m_bankfile;
        protected Bank.BankFactory.BankFactory m_factory;

        public Test_Factory(Bank.BankFactory.BankFactory factory)
            : this(factory, null)
        {
        }

        public Test_Factory(Bank.BankFactory.BankFactory factory, string bankfile)
        {
            m_factory = factory;
            m_bankfile = bankfile ?? Properties.Settings.Default.Data_Path + "banklist.txt";
        }

        public override bool runTests()
        {
            return testSomeBanks();
        }

        public virtual Boolean testSomeBanks()
        {
            printTitle("Prüfe alle Banken");
            Boolean returnV = true;
            println("Prüfe Alle Existierenden Banken");
            foreach (System.Collections.DictionaryEntry blz in getExistingBanks())
            {
                if (!m_factory.bankExists((string) blz.Key))
                {
                    returnV = false;
                    printError((string) blz.Key + " sollte Existieren!");
                }
            }
            println("Prüfe Alle Nicht Existierenden Banken");
            foreach (System.Collections.DictionaryEntry blz in getUnExistingBanks())
            {
                if (m_factory.bankExists(((decimal) blz.Key).ToString()))
                {
                    returnV = false;
                    printError(((decimal) blz.Key) + " sollte nicht Existieren!");
                }
            }
            return returnV;
        }

        protected System.Collections.Hashtable getExistingBanks()
        {
            return _getBanks("existing");
        }

        protected System.Collections.Hashtable getUnExistingBanks()
        {
            return _getBanks("unexisting");
        }

        private System.Collections.Hashtable _getBanks(string type)
        {
            System.Collections.Hashtable banks = new System.Collections.Hashtable();
            System.Collections.Hashtable banks_existing = new System.Collections.Hashtable();
            System.Collections.Hashtable banks_unexisting = new System.Collections.Hashtable();
            Decimal previous = 0;
            System.IO.FileInfo finfo = new System.IO.FileInfo(m_bankfile);

            System.IO.FileStream fp = finfo.Open(System.IO.FileMode.Open, System.IO.FileAccess.Read,
                System.IO.FileShare.Read);

            int length = Properties.Settings.Default.Bundesbank_Verfahren_Offset +
                         Properties.Settings.Default.Bundesbank_Verfahren_Length;
            byte[] byt = new byte[1024];
            int i = 0;

            int byteData;
            while ((byteData = fp.ReadByte()) != -1)
            {
                if (byteData == 10 || byteData == 13)
                {
                    string line = Encoding.Default.GetString(byt);
                    int b = 0;
                    for (b = 0; b < line.Length; b++)
                    {
                        if (line[b] == '\0')
                            break;
                    }
                    line = line.Substring(0, b);
                    byt = new byte[1024];
                    i = 0;
                    if (line.Length < length)
                        continue;

                    string blz = line.Substring(Properties.Settings.Default.Bundesbank_BLZ_Offset,
                        Properties.Settings.Default.Bundesbank_BLZ_Length);
                    if (blz == "00000000")
                        continue;
                    if (previous.ToString() != blz)
                    {
                        banks_existing.Add(blz, true);
                        for (Decimal v = previous + 1;
                            v < Decimal.Parse(blz);
                            v += Math.Ceiling((Decimal.Parse(blz) - previous)/10))
                        {
                            banks_unexisting.Add(v, true);
                        }
                        previous = Decimal.Parse(blz);
                    }
                }
                else
                {
                    byt[i] = (byte) byteData;
                    i++;
                }
            }

            fp.Close();
            banks.Add("existing", banks_existing);
            banks.Add("unexisting", banks_unexisting);
            return (System.Collections.Hashtable) banks[type];
        }
    }
}