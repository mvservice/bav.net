﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVService.BAV.Test.Factory
{
    internal class Test_Factory_BinarySearch : Test_Factory
    {
        private System.IO.FileStream m_fp;
        public Test_Factory_BinarySearch(Bank.BankFactory.BankFactoryBinarySearch factory)
            : base(factory, factory.getFile())
        {
            
            
        }
        public override bool runTests()
        {
            Boolean returnv = base.runTests();
            m_fp = new System.IO.FileStream(this.m_bankfile, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read); //finfo.Open(System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
            if (!m_fp.CanRead)
                this.kill("Konnte Datei nicht Öffnen");
            returnv = this.testLength() && returnv;
            returnv = this.testOrder() && returnv;
            m_fp.Close();
            return returnv;
        }
        public Boolean testLength()
        {
            Boolean returnv = true;
            this.printTitle("checking size");
            return returnv;
        }
        public Boolean testOrder()
        {
            Boolean returnv = true;
            this.printTitle("checking order");
            Decimal previous = 0;
            Int64 c = 0;
            int length = Properties.Settings.Default.Bundesbank_Verfahren_Offset + Properties.Settings.Default.Bundesbank_Verfahren_Length;
            byte[] byt = new byte[1024];
            int i = 0;

            int byteData;
            while ((byteData = m_fp.ReadByte()) != -1)
            {
                if (byteData == 10 || byteData == 13)
                {
                    c++;
                    string line = System.Text.Encoding.Default.GetString(byt);
                    int b = 0;
                    for (b = 0; b < line.Length; b++)
                    {
                        if (line[b] == '\0')
                            break;
                    }
                    line = line.Substring(0, b);
                    byt = new byte[1024];
                    i = 0;
                    if (line.Length < length)
                        continue;

                    string blz = line.Substring(Properties.Settings.Default.Bundesbank_BLZ_Offset, Properties.Settings.Default.Bundesbank_BLZ_Length);
                    if (decimal.Parse(blz) < previous)
                    {
                        if (blz != "00000000")
                        {
                            this.printError(blz + " ist kleiner als " + previous + " Zeile " + c + "!");
                            returnv = false;
                        }
                    }
                    previous = Decimal.Parse(blz);
                }
                else
                {
                    byt[i] = (byte)byteData;
                    i++;
                }
            }

            
            
            return returnv;
        }
    }
}
