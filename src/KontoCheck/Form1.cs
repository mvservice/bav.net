﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MVService.BAV;

namespace KontoCheck
{
    public partial class Form1 : Form
    {
        private Controller control;
        public Form1()
        {
            control = new Controller();
            InitializeComponent();
        }

        private void btEnd_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btCheck_Click(object sender, EventArgs e)
        {
            Controller control = new Controller();
            switch (control.getValidationCode(this.txtBLZ.Text, this.txtKTNR.Text))
            {
                case MVService.BAV.Validator.Validator_Result.Valid:
                    this.txtErgebnis.Text = "OK";
                    break;
                case MVService.BAV.Validator.Validator_Result.Invalid:
                    this.txtErgebnis.Text = "FALSCH";
                    break;
                case MVService.BAV.Validator.Validator_Result.UnknownBank:
                    this.txtErgebnis.Text = "Bank Unbekannt";
                    break;
                case MVService.BAV.Validator.Validator_Result.UnknownAlgorithm:
                    this.txtErgebnis.Text = "Allgoritmus Unbekannt";
                    break;

            }
            
            
            
        }



        private void btOptions_Click(object sender, EventArgs e)
        {
            MVService.BAV.Controller.ShowProperties(true);
        }

        private void txtBLZ_TextChanged(object sender, EventArgs e)
        {
            if (txtBLZ.Text.Length == 8)
            {
                this.lbBank.Text = control.getBankname(txtBLZ.Text);
            }
        }

        private void btbankname_Click(object sender, EventArgs e)
        {
            if (txtBLZ.Text.Length == 8)
            {
                control = new Controller();
                this.lbBank.Text = control.getBankname(txtBLZ.Text);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MVService.BAV.Controller.ShowTest();
        }
    }
}