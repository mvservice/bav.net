﻿namespace KontoCheck
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtKTNR = new System.Windows.Forms.TextBox();
            this.txtBLZ = new System.Windows.Forms.TextBox();
            this.btCheck = new System.Windows.Forms.Button();
            this.btEnd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtErgebnis = new System.Windows.Forms.TextBox();
            this.btOptions = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lbBank = new System.Windows.Forms.Label();
            this.btbankname = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtKTNR
            // 
            this.txtKTNR.Location = new System.Drawing.Point(90, 16);
            this.txtKTNR.Name = "txtKTNR";
            this.txtKTNR.Size = new System.Drawing.Size(100, 20);
            this.txtKTNR.TabIndex = 0;
            this.txtKTNR.Text = "0520309001";
            // 
            // txtBLZ
            // 
            this.txtBLZ.Location = new System.Drawing.Point(90, 42);
            this.txtBLZ.Name = "txtBLZ";
            this.txtBLZ.Size = new System.Drawing.Size(100, 20);
            this.txtBLZ.TabIndex = 1;
            this.txtBLZ.Text = "72012300";
            this.txtBLZ.TextChanged += new System.EventHandler(this.txtBLZ_TextChanged);
            // 
            // btCheck
            // 
            this.btCheck.Location = new System.Drawing.Point(15, 179);
            this.btCheck.Name = "btCheck";
            this.btCheck.Size = new System.Drawing.Size(75, 23);
            this.btCheck.TabIndex = 2;
            this.btCheck.Text = "Prüfen";
            this.btCheck.UseVisualStyleBackColor = true;
            this.btCheck.Click += new System.EventHandler(this.btCheck_Click);
            // 
            // btEnd
            // 
            this.btEnd.Location = new System.Drawing.Point(127, 179);
            this.btEnd.Name = "btEnd";
            this.btEnd.Size = new System.Drawing.Size(75, 23);
            this.btEnd.TabIndex = 3;
            this.btEnd.Text = "Beenden";
            this.btEnd.UseVisualStyleBackColor = true;
            this.btEnd.Click += new System.EventHandler(this.btEnd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kontonummer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "BLZ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ergebnis";
            // 
            // txtErgebnis
            // 
            this.txtErgebnis.Enabled = false;
            this.txtErgebnis.Location = new System.Drawing.Point(90, 68);
            this.txtErgebnis.Name = "txtErgebnis";
            this.txtErgebnis.ReadOnly = true;
            this.txtErgebnis.Size = new System.Drawing.Size(100, 20);
            this.txtErgebnis.TabIndex = 7;
            // 
            // btOptions
            // 
            this.btOptions.Location = new System.Drawing.Point(78, 152);
            this.btOptions.Name = "btOptions";
            this.btOptions.Size = new System.Drawing.Size(75, 23);
            this.btOptions.TabIndex = 12;
            this.btOptions.Text = "Optionen";
            this.btOptions.UseVisualStyleBackColor = true;
            this.btOptions.Click += new System.EventHandler(this.btOptions_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Bankname:";
            // 
            // lbBank
            // 
            this.lbBank.AutoSize = true;
            this.lbBank.Location = new System.Drawing.Point(87, 95);
            this.lbBank.Name = "lbBank";
            this.lbBank.Size = new System.Drawing.Size(0, 13);
            this.lbBank.TabIndex = 14;
            // 
            // btbankname
            // 
            this.btbankname.Location = new System.Drawing.Point(78, 123);
            this.btbankname.Name = "btbankname";
            this.btbankname.Size = new System.Drawing.Size(75, 23);
            this.btbankname.TabIndex = 15;
            this.btbankname.Text = "Bankname?";
            this.btbankname.UseVisualStyleBackColor = true;
            this.btbankname.Click += new System.EventHandler(this.btbankname_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(71, 236);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 283);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btbankname);
            this.Controls.Add(this.lbBank);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btOptions);
            this.Controls.Add(this.txtErgebnis);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btEnd);
            this.Controls.Add(this.btCheck);
            this.Controls.Add(this.txtBLZ);
            this.Controls.Add(this.txtKTNR);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKTNR;
        private System.Windows.Forms.TextBox txtBLZ;
        private System.Windows.Forms.Button btCheck;
        private System.Windows.Forms.Button btEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtErgebnis;
        private System.Windows.Forms.Button btOptions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbBank;
        private System.Windows.Forms.Button btbankname;
        private System.Windows.Forms.Button button1;
    }
}

